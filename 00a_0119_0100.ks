[bgload file="bg08b.png"]
[effect type = 0 time = 1000]

;//太陽が西の空に傾き、辺りはうっすらと赤い光に包まれる。
The sun is already starting to set in the western sky, creating the surroundings' reddish hue.

;//さて……何処へ行こうか。
Well then, where shall I go now?

[bgmin time=2000 file="bgm10a"]

