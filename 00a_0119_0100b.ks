[bgload file="bg59b.png"]
[effect type = 0 time = 1000]

;//吉祥寺駅の北口から闇市跡を通り抜けたその先に目指す店はあった。
Leaving the Kichijoji station via the north exit, I cut through the shopping district, which still retains traces of its black-market roots. Before me lies my destination.

[bgload file="bg33b.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm04a"]

;//純喫茶・月世界。
Moon World.

;//商店街の外れにある小さな店だ。
A simple cafe, located not far from business center.

;//珈琲と菓子が旨い、知る人ぞ知る名店だ。
Not many people know it, but their coffee and pastries are delicious.

;//まあ、己は単に昔馴染みがこの店に居るから通っているだけなのだが。
One of the reasons I visit this place is that it's run by an old friend of mine.

[se loop=0 file="se016"]

[bgload file="bg34b.png"]
[effect type = 0 time = 1000]

【???】
[v file="kyo0001"]
;//「いらっしゃい-あら、時坂くんじゃない」
"Welcome! Oh, Tokisaka-kun"

;//カウンタの中に居た女が己を見て口許を弛めた。
Seeing me said the woman behind the bar.

【Reiji】
[v file="rei0039"]
;//「己じゃ不満なのか？」
"Aren't you glad to see me?"

;//彼女-葉月杏子は肩を竦め、苦笑いを浮かべた。
In response, she, Hazuki Kyoko, shrugged and flashed a wry smile.

[bgset]
[charload num = 0 file = "haz0110b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0002"]
;//「だって時坂くんは珈琲一杯で何時までもねばるんですもの」
"You won't be satisfied until you drink some coffee, in anyway"

;//そう言いながらも杏子は機嫌が良さそうだ。
Looks like she's in a good mood.

【Reiji】
[v file="rei0040"]
;//「その珈琲を一杯頼む」
"That's right. Could I get a cup of coffee?"

[bgset]
[charload num = 0 file = "haz0111b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0003"]
;//「ええ、いいわよ。-焼き菓子もあるけれど、どう？」
"Sure. There's also some pastries, you want one?"

【Reiji】
[v file="rei0041"]
;//「いや……いや、貰おうか」
"Well no... But actually yes"

[bgset]
[charload num = 0 file = "haz0111b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//途中まで出掛かった言葉を再度否定する。
I got confused and ended up saying nonsence.

[bgset]
[charload num = 0 file = "haz0101b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0004"]
;//「ふふっ……ありがとう」
"Hehe... Alright then"

[bgload file="e101.png"]
[effect type = 0 time = 1000]

;//杏子が珈琲を淹れる。店内に芳醇な匂いが立ち籠める。
As Kyoko starts brewing coffee, the interior fills with a pleasant aroma.

【Kyoko】
[v file="kyo0005"]
;//「そういえば、さっきまで魚住くんが居たのよ」
"By the way, there was Uozumi in here earlier"

【Reiji】
[v file="rei0042"]
;//「奴が？」
"Really?"

;//魚住とは、己たちと共通の昔馴染みだ。確かにあいつもこの店の常連ではある。
All three of us are old friends. No wonder he comes here, too.

【Kyoko】
[v file="kyo0006"]
;//「仕事の途中で寄ったみたいよ」
"He seemed to be in the middle of his work"

【Reiji】
[v file="rei0043"]
;//「まああいつも忙しいだろうからな」
"Well, he surely has a lot of work to do"

;//煙草に火を付ける。
I lit a cigarette.

;//忙しいくせに、此処へは忠実に来ているようだ。
But still I'm surprised he has time for things like this.

;//尤も、忙しくさせているのは己なのだけれど。
I mean, it's me who gets him in more trouble at work.

;//目の前に白いカップが置かれた。
Before I could notice, a large white cup appeared in front of me.

[bgload file="bg34b.png"]
[effect type = 0 time = 1000]

[bgset]
[charload num = 0 file = "haz0101b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0007"]
;//「お待ちどおさま」
"Thank you for waiting"

【Reiji】
[v file="rei0044"]
;//「悪いな」
"Sorry for trouble"

[bgset]
[effect type = 0 time = 500]

;//湯気の立ち上る珈琲を啜る。砂糖もミルクも入れずに飲むのが己の流儀だ。
I bring the cup of steaming coffee to my mouth. No milk, no sugar, just as usual.

[bgset]
[charload num = 0 file = "haz0110b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0008"]
;//「はい、カステラ。好きだったでしょう？」
"Here, have some castella. I guess you like it"

【Reiji】
[v file="rei0045"]
;//「ああ」
"Yeah"

[bgset]
[effect type = 0 time = 500]

;//早速カステラを一切れ摘む。
I immediately take a piece.

[bgset]
[charload num = 0 file = "haz0201b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0009"]
;//「珈琲はブラックなのに、カステラは甘い方が好きなのよね」
"It's sweet, that's why it perfectly suits a black coffee"

【Reiji】
[v file="rei0046"]
;//「ごつい態して砂糖を何杯も入れる奴よりいいだろ」
"Well, better than putting some sugar, like Uozumi does"

[bgset]
[charload num = 0 file = "haz0201b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//そう云う奴が身近に居るのを思い出す。
I remembered, that he works somewhere close to this place.

[bgset]
[charload num = 0 file = "haz0111b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0010"]
;//「あら、飲み方は人それぞれじゃない」
"Hey, everyone drinks coffee in their own way"

【Reiji】
[v file="rei0047"]
;//「だったら己の飲み方にもケチをつけるなよ」
"Then you shouldn't judge mine"

[bgset]
[charload num = 0 file = "haz0110b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0011"]
;//「つけてないわよ？　ただ確認してみただけよ」
"I'm not judging it, okay? I'm just stating the facts"

;//杏子は笑いながらカステラの切れ端を摘んだ。
She took the remains of castella, smiling.

[bgset]
[charload num = 0 file = "haz0101b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0012"]
;//「今度新しい従業員を雇うつもりなの」
"I plan on hiring extra hands"

【Reiji】
[v file="rei0048"]
;//「この閑古鳥が鳴いている店に必要なのか？」
"In a godforsaken place like this?"

[bgset]
[charload num = 0 file = "haz0106b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0013"]
;//「もおっ……時坂くんが居ない時間はひとりだと結構忙しいのよ」
"You know... There are times when you're not around, and there're too many people to handle alone"

;//杏子が口を尖らせる。
She pouted.

[bgset]
[charload num = 0 file = "haz0110b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0014"]
;//「知り合いの娘さんでね、社会経験を積ませてあげたいんですって」
"She's my acquaintance. I want her to get some life experience"

【Reiji】
[v file="rei0049"]
;//「使えるのか？」
"Can she handle it?"

[bgset]
[charload num = 0 file = "haz0201b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0015"]
;//「余所のお店でお手伝いとかはした事あるらしいから心配ないと思うわよ」
"She used to help in another place, so yeah, I think she can"

[bgset]
[effect type = 0 time = 500]

;//余所で働いていたのならば其処で社会経験も積んでいると思うのだが。
But if she worked somewhere, she already has an experience.

;//いろいろな仕事を経験させたいと云う親心だろうか。
Looks like she aroused Kyoko's maternal instinct.

[bgset]
[charload num = 0 file = "haz0109b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0016"]
;//「……また何か考え込んでるでしょう」
"Are you brooding over something again?"

【Reiji】
[v file="rei0050"]
;//「ん-いや、大した事じゃない」
"Ah, no, nothing serious"

[bgset]
[charload num = 0 file = "haz0109b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//頬を膨らませる杏子の仕草に口許が緩む。
I smiled at pouting Kyoko.

[bgset]
[charload num = 0 file = "haz0201b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0017"]
;//「仕事のこと？」
"Is it related to your job?"

【Reiji】
[v file="rei0051"]
;//「まあ、そんな処だ」
"Well, something like that"

[bgset]
[charload num = 0 file = "haz0201b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//適当に言葉を濁す。
I dodged her question.

[bgset]
[charload num = 0 file = "haz0109b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0018"]
;//「……あまり根を詰めない方がいいわよ。疲れていると、正常な判断も出来なくなってしまうから」
"You'd better not get into it too much. When you're tired it's hard to make proper decisions"

;//杏子の言葉には深い意味が込められている事を己は知っていた。
I sensed a deep thought in Kyoko's words.

[bgset]
[effect type = 0 time = 500]

;//彼女は七年前に旦那を亡くしている。この『月世界』のマスターだった彼は、連日の仕事で疲労困憊していた時に自動車事故に遭った。
7 years ago she lost her husband. He owned this cafe. One day, severely fatigued from everyday work, he got into an accident.

;//相手は武蔵野グリーンパークに駐留していた在日米軍将校だったらanywayしい。そして杏子の旦那は亡くなった。
Other person involved in it was an american soldier. Kyoko's husband died.

;//交通事故死。そう公的書類には残された。
Accidental death. So said official documents.

;//たったそれだけだ。事故の調査も米軍が行い、当時警察官だった己の元にすら詳しい調査結果はやってこなかった。
And nothing else. Investigation was also held by americans. Even for me, the police officer those years, there were too many blind spots in reports.

;//それどころか-旦那の遺体すら返還されなかった。杏子の哀しみは如何ほどだったろうか。
More than that, they didn't return the body to the victims family. Can you imagine what it was like for Kyoko?

;//同じ頃己も両親を飛行機事故で亡くしていたが、己の場合は遠い地で起こった出来事だった所為かあまり衝撃は受けなかった。
I wasn't really shocked by it. Maybe it's because I've lost my parents in a plane crash.

;//-ともあれ、杏子の旦那の死因に不審を覚えた己は、警官と云う立場を利用して真相の究明に乗り出した。
The reason her husband died was unknown anyway, so I started an investigation of my own, using police authorities.

;//ただそれも、全くの徒労に終わった。米軍からは一切の資料の提出を拒否され、挙げ句の果てには強引に捜査を打ち切られてしまった。
Nevertheless all this was for nothing. Americans declined cooperation and forced me to drop it.

;//-これが敗戦国の実状なのだろう。まだ終戦から四年しか過ぎていなかった当時では寧ろ当然の事だったのかもしれない。
Because of the situation in our country defeated in war. Not even 4 years has passed since then, so that was where we were.

[bgset]
[charload num = 0 file = "haz0108b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0019"]
;//「……まだ何か考えてる」
"You're daydreaming again"

;//杏子の指が己の眉間を突いた。
Kyoko knocked my forehead with her finger.

【Reiji】
[v file="rei0052"]
;//「いや、随分と良い時代になったと思ってな」
"No, just thought that we're all feeling much better"

[bgset]
[charload num = 0 file = "haz0203b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0020"]
;//「良い時代……かしら？」
"You think so?"

;//杏子が首を傾げる。
She tilt her head slightly.

【Reiji】
[v file="rei0053"]
;//「少なくとも、十年前から比べれば良くはなっているだろう？」
"I mean better than 10 years ago"

[bgset]
[charload num = 0 file = "haz0201b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0021"]
;//「……比べてしまえば、そうかもしれないわね」
"Maybe you're right"

【Reiji】
[v file="rei0054"]
;//「あの頃の方が楽しかったかもしれないけれどな」
"But there were good times either"

[bgset]
[charload num = 0 file = "haz0102b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0022"]
;//「……由記子が居たから？」
"With Yukiko?"

【Reiji】
[v file="rei0055"]
;//「-さあな」
"Perhaps"

[bgset]
[effect type = 0 time = 500]

;//杏子の問いをはぐらかし、己は煙草に火を付けてそっぽを向いた。
Not giving a straight answer, I lit another cigarette and turn my back.

[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0119_0100z.ks"]
[end]
[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0119_0200.ks"]
[end]
