;//まだ調べたりないような気がする。
I'm not done yet.

[bgload file="te003.png"]
[effect type = 0 time = 1000]

[bgload file="bg55k.png"]
[effect type = 0 time = 1000]

【Uozumi】
[v file="uoz0039"]
;//「目撃者は居るのか？」
"We got any witnesses?"

【Officer】
[v file="kea0002"]
;//「第一発見者は今朝公園内を散歩していた近所のお年寄りです。この人は昨夜もこの辺りに居たらしく、その時は何もなかったと言っています」
"There's an old man who discovered the body during his walk through the park. He said that the evening before that happened he'd also been here and seen nothing suspicious"

【Uozumi】
[v file="uoz0040"]
;//「……すると、夜中のうちに運ばれてきたって事か」
"Which means criminal brought the body during the night..."

;//地面に血痕が見当たらないから、この場でばらしたとは考えにくい。
Since there're no bloodstains on earth, it's a bit hard to imagine.

;//深夜となると電車も動いていない。近場で殺されたのか、それとも乗用車で運ばれてきたのか-
When night falls, train services stop. Did the murder happened nearby, or maybe criminal drives a car---- 

【Uozumi】
[v file="uoz0041"]
;//「こんなもんか……」
"I see..."

;//今ここで調べられるのはこの程度だろう。
Looks like there's nothing else left for me here.

;//あとは鑑識に任せるか-
I'll leave other things to forensics.

[bgload file="bg61d.png"]
[effect type = 0 time = 1000]

;//大方調べ終え、魚住は現場を離れて煙草に火を付けた。
Done with examination, Uozumi left the crime scene and lit a cigarette.

【Uozumi】
[v file="uoz0042"]
;//「……ホトケは解剖に回して身許その他を調べて貰え。あとは周辺の聞き込みは任せたぞ」
"Send the body for autopsy and identify the victim. Then canvass the neighbourhood"

【Forensics Member】
[v file="kan0007"]
;//「わかりました」
"Understood"

【Officer】
[v file="kea0003"]
;//「了解であります」
"Yes, sir!"

;//鑑識員が遺体の収集を始め、警官が走り去る。
Forensics member began collecting the body, the officer ran away.

;//現場で出来るのはこのくらいだ。
My work here is done.

;//木々の切れ目から町工場の建物が見えた。今年に入ってすぐ、発砲事件が起きた処だ。
Through the trees can be seen the factory, where the shooting incident happened not long ago. 

;//あの事件は結局時坂の奴が予想した通りだった。池からは拳銃が発見され、被害者の息子をちょっと締め上げるとすぐに父親を殺したと自供した。
Everything transpired just as Reiji expected. We found the pistol in the pond, and after we pressed on the victim's son, he spilled the beans.

;//警察としてあまり褒められた捜査の仕方ではない。それでも犯人を取り逃がすよりはずっとましだと魚住は考えていた。
For police it's not the best way to get confessions, but it's better than leaving a criminal walk free. That's how Uozumi thought.

;//結局動機は金だった。そんな処まで時坂に予想されていたのは腹が立つものの、それでも事件は解決した。
In the end the son was going after his father's money. The fact that Reiji guessed everything right pissed off Uozumi. However, the case was already closed.

【Uozumi】
[v file="uoz0043"]
;//「……このヤマにも食いついてきそうだな……」
"...Seems this time too he succeeded..."

;//時坂玲人と云う男は何故か殺人事件等の兇悪犯罪に固執する嫌いがあった。もしかしたら奴が警察を辞めた事件とも関係があるのかもしれない。
For some reason Tokisaka Reiji always hated investigating violent murder cases. Maybe, the reason he left police was that case...

;//それ以上魚住は考えないことにした。
Uozumi stopped himself from thinking about that any further.

;//奴の事ばかりではなく、考える事全般がどうも苦手だった。刑事とは足で調べるもの、と云う先輩刑事の教えが魚住に深く根付いているのもある。
It's not only Reiji's case, besides Uozumi didn't like thinking too much. Once his senpai taught him that policeman investigates with his legs. This thought took a deep root in Uozumi's mind.

;//だからと言って、魚住が猪突猛進で物事にあたると云う訳ではない。そのくらいの分別はある。
Nevertheless, it didn't mean he made decisions thoughtless and on impulse. In some degree Uozumi was very cautious.

;//とはいえ-
But----

【Uozumi】
[v file="uoz0044"]
;//「……次からは玲人の奴に任せるか」
"Should I refer this case to Reiji?"

;//餓鬼の頃より、考えるのはあいつの仕事だ。
Ever since our childhood thinking stuff was his job.

[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0229_0200.ks"]
[end]
