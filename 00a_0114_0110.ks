[bgload file="bg17a.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm02a"]

;//綴子と別れ、紫は美術室へやってきた。
After bidding farewell to Tsuzuriko, Yukari headed to the art club.

[bgset]
[charload num = 0 file = "yui0101a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Student】
[v file="yui0001"]
;//「あっ、時坂先輩おはようございます」
"Ah, Tokisaka-sempai, good morning"

;//先に美術室に来ていた女学生が立ち上がり、紫に挨拶をした。西園唯、美術部の後輩だ。
A girl who arrived earlier greeted her. The girl's name was Nishizono Yui.

【Yukari】
[v file="yuk0024"]
;//「おはよう、西園さん」
"Good morning, Nishizono-san"

[bgset]
[effect type = 0 time = 500]

;//挨拶を返し、紫は自分の道具箱を机に置いた。
After saying hello, Yukari arranged her tools on the desk.

[bgset]
[charload num = 0 file = "yui0102a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0002"]
;//「時坂先輩も始業式から部活に出るんですね」
"Tokisaka-sempai, you too came here right after the opening ceremony?"

【Yukari】
[v file="yuk0025"]
;//「家に居る間全然描けなかったから」
"I didn't have time to draw when I was at home"

[bgset]
[effect type = 0 time = 500]

;//鉛筆を取り出し、芯を確認する。少し肥後守で削った方がいいかもしれない。
She took her pencil and checked the lead. Perhaps, it needed some sharpening.

[bgset]
[charload num = 0 file = "yui0103a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0003"]
;//「忙しかったんですか？」
"Were you busy?"

【Yukari】
[v file="yuk0026"]
;//「そうね……物臭な兄が年末年始に何もしてくれなかったから」
"Yeah... During the holidays my lazy brother slacked off too much"

[bgset]
[charload num = 0 file = "yui0104a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0004"]
;//「……大変ですね」
"It must be difficult"

[bgset]
[effect type = 0 time = 500]

;//唯は軽く肩を竦めると、自分の画材道具を机に拡げだした。
Yui shrugged and arranged her tools on the table.

【Yukari】
[v file="yuk0027"]
;//「両親が亡くなってから随分経つから、もう慣れてしまったけれどね」
"It's been many years since our parents died. I got used to it"

[bgset]
[charload num = 0 file = "yui0105a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0005"]
;//「そうだったんですか……」
"I see..."

【Yukari】
[v file="yuk0028"]
;//「気にしなくてもいいのよ」
"You don't have to worry about it"

[bgset]
[charload num = 0 file = "yui0102a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0006"]
;//「いえ……私も、似たような-」
"It's not that... I mean, I also..."

[se loop=0 file="se011"]

[bgset]
[effect type = 0 time = 500]

;//唯の言葉の途中で美術室の扉が音を立てて開かれた。
Before she could finish, the door to the club had opened.

[bgset]
[charload num = 0 file = "ayu0304a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Student】
[v file="ayu0001"]
;//「唯居る？　……あ、時坂先輩」
"Yui, are you here? ...Ah, Tokisaka-sempai"

;//貌を覗かせた女学生が紫を見て頭を下げた。
The girl who entered the room bowed her head.

;//剣道部の一年、佐東歩だ。唯と仲が良いらしく、よくこの美術部にも顔を出している。
It was Sato Ayumu from kendo club. It seemed, she was friends with Yui and came often.

;//必然的に紫や他の部員とも顔見知りになっていた。
That's why she knew Yukari and her fellow club members.

【Yukari】
[v file="yuk0029"]
;//「佐東さん、剣道部は？」
"Sato-san, how's your club?"

[bgset]
[charload num = 0 file = "ayu0305a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Ayumu】
[v file="ayu0002"]
;//「今日は休みなんですよ」
"We're having a break"

;//歩は鞄を置きながら答えた。
She said, taking her bag off her shoulder.

[bgset]
[charload num = 0 file = "ayu0305a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Ayumu】
[v file="ayu0003"]
;//「冬休み中ずっと部活がありましたから、始業式くらいは休みにしようって」
"Since we trained hard during winter holidays, they allowed us to rest at the beginning of semester"

【Yukari】
[v file="yuk0030"]
;//「ずっと……お正月も？」
"Even in January?"

[bgset]
[charload num = 0 file = "ayu0305a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Ayumu】
[v file="ayu0004"]
;//「流石に元日はお休みでしたけれど、二日からはずっとです」
"We did rest on the first day, of course, but continued our training on the second"

;//平然とした様子で歩は言う。
She replied calmly.

[bgset]
[charload num = 0 file = "yui0103a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

[bgset]
[charload num = 0 file = "ayu0305a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0007"]
;//「剣道部って大変なのね」
"Going to kendo club must be difficult"

[bgset]
[charload num = 0 file = "ayu0307a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Ayumu】
[v file="ayu0005"]
;//「部活動だけだったらいいのだけれど……ずっと先輩に引っ張り回されていたから」
"It wouldn't be, if only it was just club activities... Our sempais were following us all the time"

[bgset]
[charload num = 0 file = "yui0104a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0008"]
;//「ああ……歩は人気があるもの、仕方ないと思うわよ」
"Ah, it's because you're very popular. There's nothing you can do about it"

[bgset]
[charload num = 0 file = "ayu0105a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Ayumu】
[v file="ayu0006"]
;//「……お手洗いにまで付いて来なくてもいいと思うんだけど……」
"Even so, following you to a toilet is a bit too much..."

[bgset]
[effect type = 0 time = 500]

;//紫にはよく判らなかったが、そう云う趣味の人も居るのだろう。
Yukari couldn't understand that, but such people really existed.

【Yukari】
[v file="yuk0031"]
;//「美術部は休み中に集まる事は無かったわね。自主活動だけで」
"We didn't even gathered together during the holidays. Everyone had individual activities"

;//まあそれも、紫は全く行えなかったのであるが。
Although Yukari had no activities at all.

[bgset]
[charload num = 0 file = "yui0101a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

[bgset]
[charload num = 0 file = "ayu0105a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0009"]
;//「私は少しだけだったら……みかんとか、鏡餅とかのスケッチだけですけれど。あっ、そうだ歩、今度モデルをやってくれないかな？」
"I had drawn a few things like mandarin or kagami mochi... By the way, Ayumu, wanna pose for my next painting?"

[bgset]
[charload num = 0 file = "ayu0304a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Ayumu】
[v file="ayu0007"]
;//「モデル……？」
"Being a model, huh?"

[bgset]
[charload num = 0 file = "yui0102a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0010"]
;//「ええ、勿論ヌードでね。先輩たちには評判になると思うんだ」
"Yes, and of course it'll be nude painting. Such things are very popular among sempais"

[bgset]
[charload num = 0 file = "ayu0104a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Ayumu】
[v file="ayu0008"]
;//「別にいいけど……高いよ？」
"Well, I don't know... How much will you pay me?"

[bgset]
[charload num = 0 file = "yui0105a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0011"]
;//「いくらなら脱ぐ？」
"How much do you need to get undressed?"

[bgset]
[charload num = 0 file = "ayu0306a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Ayumu】
[v file="ayu0009"]
;//「うーん……二万円くらいかな。ほら、去年トランジスタラジオが発売されたじゃない。あれがそのくらいだったから」
"Hmm... 20 000 yen, I guess. Last year they started selling broadcast receivers. They cost about the same"

[bgset]
[charload num = 0 file = "yui0107a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0012"]
;//「ラジオかぁ……私だったらテレビの方が欲しいかなぁ」
"Radio? And I'd like a TV"

【Yukari】
[v file="yuk0032"]
;//「テレビよりも先月出た電気炊飯器が欲しいわ。兄さんに頼もうかしら」
"I'd like a rice cooker they put on sale last month. Maybe I should ask my brother to buy it"

[bgset]
[charload num = 0 file = "yui0107a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

[bgset]
[charload num = 0 file = "ayu0305a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Ayumu】
[v file="ayu0010"]
;//「先輩って家庭的ですよね」
"Sempai, you always put family first"

【Yukari】
[v file="yuk0033"]
;//「家に居るとそうなってしまうのよ」
"If you spend most of your time at home, it's inevitable"

[bgset]
[charload num = 0 file = "yui0108a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

[bgset]
[charload num = 0 file = "ayu0305a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0013"]
;//「……かもしれませんね」
"Maybe..."

;//唯が頷く。
Yui bowed.

【Yukari】
[v file="yuk0034"]
;//「家に居るときでも時間を見つけて蝸牛を描いていればよかったわ」
"I wish I have time to draw snails at home"

[bgset]
[charload num = 0 file = "yui0107a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

[bgset]
[charload num = 0 file = "ayu0305a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0014"]
;//「蝸牛……ですか」
"S-snails?"

;//何故か唯が数歩後退った。
For some reason she took a few steps back.

【Yukari】
[v file="yuk0035"]
;//「ええ。突然変異で右巻きになったヒダリマキマイマイを飼っているの」
"Yeah. I keep Euhadra quaesita, that have their shells clockwise due to spontaneous mutations"

[bgset]
[charload num = 0 file = "yui0107a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

[bgset]
[charload num = 0 file = "ayu0302a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Ayumu】
[v file="ayu0011"]
;//「右巻きで左巻き……え？」
"Clockwise... Euha... What?"

;//歩が目を瞬かせる。
Ayumu started blinking like crazy.

[bgset]
[charload num = 0 file = "yui0107a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0015"]
;//「……時坂先輩は虫とかが好きだから」
"Tokisaka-sempai really likes insects"

;//唯が苦笑しながら取り成した。
With a crooked smile Yui explained things to Ayumu.

【Yukari】
[v file="yuk0036"]
;//「春になればもっと描けるのに。家の近くに大きな池があるから、そこでゲンゴロウやタニシを集めるのよ」
"When spring comes I will have more things to draw. We have a large pond near our house, where I collect diving beetles and pond snails"

[bgset]
[charload num = 0 file = "yui0104a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

[bgset]
[charload num = 0 file = "ayu0307a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0016"]
;//「ゲンゴロウって……なんだかゴキブリみたいじゃありません……？」
"These diving beetles... they're like cockroaches, aren't they?"

【Yukari】
[v file="yuk0037"]
;//「全然違うわよ？　ゲンゴロウは甲虫だし、ゴキブリに近いのはむしろカマキリやシロアリだから」
"No way. Diving beetles are Dytiscidae. Cockroaches are more similar to mantises or termites"

[bgset]
[charload num = 0 file = "yui0104a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

[bgset]
[charload num = 0 file = "ayu0302a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Ayumu】
[v file="ayu0012"]
;//「シロアリは蟻じゃないんですか？」
"I thought termites and ants are the same insect"

【Yukari】
[v file="yuk0038"]
;//「群体で行動する処が似ているだけよ。蟻の幼虫は蛆虫のような形だけれど、シロアリは成虫に近い外見で産まれてくるの」
"They're alike only in forming colonies. Ant's larvae resemble maggots, and termites look like an adult insect right from start"

[bgset]
[charload num = 0 file = "yui0108a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

[bgset]
[charload num = 0 file = "ayu0302a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0017"]
;//「うわぁ……あまり聞きたくないです……」
"Uwaaa... Please, no more..."

;//唯が耳を押さえてしまった。
Yui closed her ears with hands.

【Yukari】
[v file="yuk0039"]
;//「昆虫学の勉強にもなると思うのだけど、残念ね」
"Sorry. I plan to study entomology in future"

[bgset]
[charload num = 0 file = "yui0108a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

[bgset]
[charload num = 0 file = "ayu0105a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Ayumu】
[v file="ayu0013"]
;//「……そう云う問題ではないと思います」
"I don't think you'll have any problems with that"

【Yukari】
[v file="yuk0040"]
;//「そう？」
"You think so?"

[bgset]
[charload num = 0 file = "yui0104a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

[bgset]
[charload num = 0 file = "ayu0104a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0018"]
;//「とっ、とにかく……何処かにスケッチに行ければいいのですけれどね」
"A-anyway... It doesn't matter what to draw, as long as you're happy with it"

;//唯は昆虫が苦手なのだろう、話を逸らしてきた。
It seems Yui doesn't like insects, that why she changed the topic.

【Yukari】
[v file="yuk0041"]
;//「まだ寒いから、写生をするなら春になってからかしらね」
"It's still cold, so we should probably wait until spring to be able to draw outside"

[bgset]
[effect type = 0 time = 500]

;//紫は窓の外に視線を移した。雪こそ積もっていないものの、木々は葉を散らし、寒そうに震えている。
Yukari looked out the window. There wasn't too much snow, but the sight of leafless trees gave her shivers.

[bgset]
[charload num = 0 file = "ayu0304a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Ayumu】
[v file="ayu0014"]
;//「ですけど、さっき校庭で朽木先輩が絵を描いていましたよ」
"But a while ago I saw Kuchiki-sempai drawing at the schoolyard"

【Yukari】
[v file="yuk0042"]
;//「朽木さんが……？」
"Kuchiki-san?"

[bgset]
[charload num = 0 file = "ayu0304a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//彼女が独りで絵を描いているのはいつもの事なのだけれど、先程まで保健室で休んでいたのに、もう恢復したのだろうか。
She regularly draws alone, however not long ago she was at the infirmary. Yukari wondered if she recovered completely.

【Yukari】
[v file="yuk0043"]
;//「水原さんは居なかった？」
"Wasn't Mizuhara-san with her?"

[bgset]
[charload num = 0 file = "ayu0306a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Ayumu】
[v file="ayu0015"]
;//「水原先輩ですか？　……すみません、判らないです」
"Mizuhara-sempai? Sorry, I'm haven't seen her"

;//すまなそうに歩は頭を下げる。
Ayumu hung her head.

【Yukari】
[v file="yuk0044"]
;//「……そう」
"I see"

[bgset]
[effect type = 0 time = 500]

;//寒い中、冬子ひとりで大丈夫だろうか。紫は外へ向かおうと立ち上がった。
It's still winter, will she be alright? Yukari decided to check on her.

[se loop=0 file="se011"]

[bgset]
[charload num = 0 file = "tat0103a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kusaka】
[v file="kus0004"]
;//「時坂さん、丁度良かった」
"Tokisaka-san, I'm glad you're here"

【Yukari】
[v file="yuk0045"]
;//「日下先生……？」
"Kusaka-sensei?"

[bgset]
[charload num = 0 file = "tat0103a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//美術室に入ってきた日下とぶつかりそうになり、紫は咄嗟に身体を反らした。
Yukari narrowly escaped bumping into her teacher, who just entered the room.

[bgset]
[charload num = 0 file = "tat0104a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kusaka】
[v file="kus0005"]
;//「先程の生活指導の印刷物を渡すのを忘れていましたから、これを」
"I forgot to give you the printout from today's briefing"

【Yukari】
[v file="yuk0046"]
;//「あ……わざわざありがとうございます、先生」
"Oh... Thank you so much, sensei"

[bgset]
[effect type = 0 time = 500]

;//プリントを受け取り、礼を述べる。
Yukari took the papers and bowed.

[bgset]
[charload num = 0 file = "tat0101a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kusaka】
[v file="kus0006"]
;//「朽木さんは……まだ保健室ですか？」
"Is Kuchiki-san still at the infirmary?"

;//美術室の中を見回しながら日下が訊ねてくる。
He asked, looking around the room.

【Yukari】
[v file="yuk0047"]
;//「佐東さんの話では校庭にいるようですけれど、私が渡しましょうか？」
"Sato-san says that she's drawing at the schoolyard. Should I relay a printout to her?"

[bgset]
[charload num = 0 file = "tat0102a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kusaka】
[v file="kus0007"]
;//「そうですね」
"I'd appreciate that"

;//日下はもう一枚プリントを紫に手渡した。
Kusaka gave Yukari another copy.

[bgset]
[charload num = 0 file = "tat0104a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kusaka】
[v file="kus0008"]
;//「それではよろしくお願いします、時坂さん」
"I'm counting on you, Tokisaka-san"

[bgset]
[effect type = 0 time = 500]

;//日下が背を向けて美術室を出て行った。
After that he left the club room.

【Yukari】
[v file="yuk0048"]
;//「ちょっと朽木さんの処へ行ってくるわね」
"Perhaps, I should check on Kuchiki-san"

;//朦朧としていた唯が面を上げる。
Yui absently raised her head.

[bgset]
[charload num = 0 file = "yui0101a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yui】
[v file="yui0019"]
;//「あ……はい。わかりました」
"Ah, sure"

【Yukari】
[v file="yuk0049"]
;//「それじゃお留守番よろしくね、ふたりとも」
"Well, good luck to you two"

[bgset]
[effect type = 0 time = 500]

;//唯と歩に声を掛け、紫は校庭へ向かった。
After bidding farewell to both girls Yukari left the room.

[bgload file="bg21a.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm02b"]

;//凩に似た冷たい風が広い校庭を吹き抜けた。
A cold winter breeze was blowing through the campus.

;//外套を着て来れば良かったと思いつつ、紫は校舎の陰になっている一角を目指した。
Wishing she had put on her coat Yukari headed for the corner of the schoolyard.

;//其処に、画架を前にして座る冬子が居た。
Toko was sitting in front of the canvas.

;//真剣な横顔。紫は声を掛ける処か近付く事すら出来ず、その場に立ち尽くしてしまった。
She had serious expression on her face. Yukari stood still, hesitant to say anything or move closer.

;//透子も居ない。恐らく邪魔にならないようにしているのだろう。
Mizuhara Toko was nowhere to be seen. Perhaps, she didn't want to get in the way.

;//でも-冬子の前のカンバスはまだ白いままだった。
Nonetheless, the canvas in front of Kuchiki Toko was pure white.

【Yukari】
[v file="yuk0050"]
;//「-朽木さん」
"Kuchiki-san"

[bgset]
[charload num = 0 file = "huy0501a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="kto0014"]
;//「……ああ、時坂さんか」
"Ah, Tokisaka-san"

;//声を掛けた紫を冬子が見上げてくる。
After hearing Yukari's voice, Toko looked up at her.

【Yukari】
[v file="yuk0051"]
;//「プリントを渡しておいてと頼まれたから」
"I was asked to give this printout"

[bgset]
[charload num = 0 file = "huy0107a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="kto0015"]
;//「ありがとう」
"Thank you"

【Yukari】
[v file="yuk0052"]
;//「もう大丈夫なの？」
"Are you all right?"

[bgset]
[charload num = 0 file = "huy0109a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="kto0016"]
;//「時坂さんには心配を掛けてしまったようだね」
"Looks like I really made you worried"

[bgset]
[effect type = 0 time = 500]

;//紫の貌を仰ぎ見、薄く笑う。
Glancing at Yukari, she flashed a weak smile.

;//それは、同性の紫が見ても一瞬心を奪われてしまいそうになる表情だった。
Judging by her face, she was touched.

;//男性的な-少年のような口振りだからだろうか。
Maybe that's why her answer was in men's style.

;//彼女の容姿はどこからどう見ても、たおやかな少女のものなのに。
But no matter how you look at it, she was an elegant girl.

;//だからその隔たりに惹かれてしまうのだろうか。
Yukari thought that this difference between looks and behaviour made Toko attractive.

[bgset]
[charload num = 0 file = "huy0501a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="kto0017"]
;//「……まだ何か用？」
"Anything else?"

【Yukari】
[v file="yuk0053"]
;//「……何を描こうとしているのか、少し気になって」
"...I was wondering what are you drawing"

[bgset]
[charload num = 0 file = "huy0503a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="kto0018"]
;//「そうだね……私にもまだ解らないよ」
"Let's see... I don't really understand it either"

;//白いカンバスを眺め、冬子は呟いた。
She muttered looking at the empty canvas.

[bgset]
[charload num = 0 file = "huy0109a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="kto0019"]
;//「描きたいものがまだ見つかっていないのかもしれないね」
"I guess I have yet to figure out what I want to draw"

【Yukari】
[v file="yuk0054"]
;//「焦る必要はないと思うけれど」
"I think there's no need to hurry"

[bgset]
[effect type = 0 time = 500]

;//課題が出されている訳でもない。
Their conversation didn't go well.

;//紫や冬子が所属している美術部は、単に絵を描くのが好きな学生が集まっている場所だ。
The art club they joined was just a place, where students fond of drawing could gather. And nothing more.

;//楽しんで絵を描く事が出来たらそれでいい、そう顧問の佐伯教頭先生は仰っていた。
It's good as long as you can enjoy drawing. So said their club advisor and academy's vice-principal Saeki-sensei.

[bgset]
[charload num = 0 file = "huy0107a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="kto0020"]
;//「……そうだね、ゆっくり考えるかな」
"Right. I should probably give it further thought"

[bgset]
[effect type = 0 time = 500]

;//鉛筆を筆箱に仕舞い、冬子が立ち上がる。
Toko put away her pencil and stood up.

[bgset]
[charload num = 0 file = "huy0101a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="kto0021"]
;//「時坂さんは、これからどうするの？」
"What are you planning to do now?"

【Yukari】
[v file="yuk0055"]
;//「私は、新宿に居る兄の処に行くつもりだけれど」
"I was going to visit my brother in Shinjuku"

[bgset]
[charload num = 0 file = "huy0101a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//紫は何とはなしに考えていた事を話した。
For some reason she said out loud what was on her mind.

[bgset]
[charload num = 0 file = "huy0501a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="kto0022"]
;//「-そうか。お兄さんは探偵だったっけ？」
"Interesting. I heard your brother is a detective"

【Yukari】
[v file="yuk0056"]
;//「ええ-家ではあまり役に立たないけれど」
"Yes. Yet in household he's not much of a helper"

[bgset]
[charload num = 0 file = "huy0501a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//苦笑しながら、紫は冬子の問いに答えた。
She replied with a crooked smile.

[bgset]
[charload num = 0 file = "huy0507a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="kto0023"]
;//「ふうん……まあいいや。私はもう行くよ。時坂さん、また明日」
"Hmm... Alright. I have to go. See you, Tokisaka-san"

【Yukari】
[v file="yuk0057"]
;//「ええ、また明日」
"See you tomorrow"

[bgset]
[charload num = 0 file = "huy0507a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0114_0200.ks"]
[end]
