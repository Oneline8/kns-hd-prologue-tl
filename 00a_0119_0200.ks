[bgload file="bg02h.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm10a"]

;//日が沈み、晩飯を何処で食うか考えていた時だった。
The sun went down. I was thinking where I could grab a bite.

[se loop=0 file="se009"]

[bgset]
[charload num = 0 file = "kyo0103a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【???】
[v file="uoz0001"]
;//「おう玲人、邪魔するぜ」
"Yo, Reiji. Sorry for intruding."

;//突然扉が開き、大柄な男が入ってきた。
Suddenly the door opened, and a large man entered the room.

【Reiji】
[v file="rei0062"]
;//「なんだ魚住、いきなりだな」
"Ah, Uozumi. I wasn't expecting you"

[bgset]
[charload num = 0 file = "kyo0103a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//こいつは魚住夾三。こんな犯罪者然とした人相でも一応警視庁の刑事だ。
This big fella, Uozumi Kyozo, who looked like a criminal, worked in Tokyo MPD.

;//ついでに言うと、餓鬼の頃から戦争の時も警官の時も一緒だった腐れ縁だ。
We spent together our childhood, survived through war and even worked in police.

;//いい加減厭気がさして己は先に警察を辞めてしまったのだが。
But later on I retired from there.

【Reiji】
[v file="rei0063"]
;//「何しに来た？」
"Why did you come?"

[bgset]
[charload num = 0 file = "kyo0102a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0002"]
;//「ご挨拶だな、テメエに頼まれたブツを持ってきたんだろうが」
"What kind of greeting is this? I brought what you asked for"

[bgset]
[effect type = 0 time = 500]

[se loop=0 file="se055"]

;//魚住は小脇に抱えていた封書をテーブルの上に投げ出した。
He throwed an envelope on the table.

【Reiji】
[v file="rei0064"]
;//「例の組の名簿か？」
"Is this the list of names?"

[bgset]
[charload num = 0 file = "kyo0204a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0003"]
;//「そうだ。本来は持ち出し禁止なんだからな。丁寧に扱ってくれよ」
"Right. Actually it's forbidden to take any documents from department, so take a good care of it"

【Reiji】
[v file="rei0065"]
;//「そう言うんだったら投げ出すんじゃねえよ」
"Of course I'm not going to throw it away"

[bgset]
[effect type = 0 time = 500]

;//封筒の中から分厚い紙束を取り出す。
I took a large pack of documents from the envelope.

[bgset]
[charload num = 0 file = "kyo0207a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0004"]
;//「本当にこの中に犯人が居るのか？」
"Are sure there's a criminal among these people?"

;//名簿を捲る己の手元を覗き込みながら魚住が訊いてくる。
Asked Uozumi, looking how I'm going through the papers

【Reiji】
[v file="rei0066"]
;//「多分居ないだろうな」
"Looks like there isn't"

[bgset]
[charload num = 0 file = "kyo0207a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//紙束の端を指でなぞりながら魚住に答える。
I answer him after checking the whole pack.

[bgset]
[charload num = 0 file = "kyo0106a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0005"]
;//「待ちやがれ玲人！　じゃあ何でこんなモンを持って来させたんだ！？」
"Wait, what? Then why did I carry around this envelope!?"

【Reiji】
[v file="rei0067"]
;//「-要らないと連絡するのを忘れていた」
"I forgot to tell you I don't need it anymore"

[bgset]
[charload num = 0 file = "kyo0102a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0006"]
;//「けっ！！」
"Tsk!"

[bgset]
[effect type = 0 time = 500]

[se loop=0 file="se033"]

;//魚住が盛大に舌打ちをして己の座るソファを蹴飛ばしてくる。
Сlicking his tongue, Uozumi kicked the couch I was sitting on.

;//埃が飛ぶから止めてくれ。
Dust raised in the air.

[bgset]
[charload num = 0 file = "kyo0202a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0007"]
;//「じゃあ何か？　犯人の目星は全くついてないってのか！？」
"What now? Don't you have any idea who's the criminal?"

【Reiji】
[v file="rei0068"]
;//「いや、その点は心配するな」
"I have. Don't worry too much" 

[bgset]
[charload num = 0 file = "kyo0203a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0008"]
;//「何か解った事があるのか聞きに来た」
"I came to ask if you have any new information" 

【Reiji】
[v file="rei0069"]
;//「何かあったらこっちから連絡を入れるさ」
"I would give you a call then"

[bgset]
[charload num = 0 file = "kyo0206a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0009"]
;//「まだ何も解っちゃいねえってのか？」
"So, you still didn't get anything?"

【Reiji】
[v file="rei0070"]
;//「いやいや、薄々と解って来てはいるんだがな」
"Well, I found something..."

[bgset]
[effect type = 0 time = 500]

;//己は煙草に火を付けた。
I lit a cigarette.

【Reiji】
[v file="rei0071"]
;//「現場に問題があった。被害者の町工場の周辺は既に更地になっている。そこで発砲などあればどうしても周囲に響き渡る」
"I visited the crime scene. There are no buildings around the factory, where victim was found. If someone used firearm, anyone could hear it"

[bgmin time=2000 file="bgm07"]

[bgset]
[charload num = 0 file = "kyo0207a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0010"]
;//「まあ……そうだな。現に付近の住人も発砲音を聞いている」
"Well... that's right. His neighbours heard the shooting"

【Reiji】
[v file="rei0072"]
;//「だろう？　何事かと思って外に出た人も居るだろうな。……現場から立ち去る人影を見た奴は居るのか？」
"Really? Then there must be someone who went outdoors after hearing a gunshot. Isn't there anyone who saw suspicious person leaving the crime scene?"

[bgset]
[effect type = 0 time = 500]

;//魚住が首を振る。
Uozumi shook his head.

[bgset]
[charload num = 0 file = "kyo0106a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0011"]
;//「……目撃者が居ればテメエに捜査なんぞ頼まん」
"...If there was an eyewitness, why would I need your help?"

【Reiji】
[v file="rei0073"]
;//「貌は見えなくとも背格好である程度は犯人を割り出せるだろうからな」
"Even if you haven't seen the face of the suspect, offender profiling is still possible using other body parameters"

[bgset]
[effect type = 0 time = 500]

;//煙草の灰を灰皿に落とす。
I put an ash in the ashtray.

【Reiji】
[v file="rei0074"]
;//「警察に通報したのは被害者の家族か？」
"Was it victim's family who reported police?"

[bgset]
[charload num = 0 file = "kyo0202a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0012"]
;//「ああ。ガイシャと同居している息子夫婦だ。息子が倒れているガイシャを発見し、その嫁が警察に通報してきた」
"Yeah. Victim was living with his son's family. The son was the one who discovered the body. His wife called the police"

【Reiji】
[v file="rei0075"]
;//「……その息子夫婦は工場の売却には賛成していたようだな」
"Did you know that these two put the factory on sale?"

[bgset]
[charload num = 0 file = "kyo0107a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0013"]
;//「-おい玲人」
"----Wait a moment, Reiji"

;//魚住の声に凄みが増す。
These was a tension in his voice.

[bgset]
[charload num = 0 file = "kyo0101a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0014"]
;//「息子は調布飛行場の米軍基地で働く真っ当な人間だぞ」
"His son works on american army facility"

【Reiji】
[v file="rei0076"]
;//「-つまり、拳銃程度ならいくらでも入手出来るな」
"In other words it's easy for him to obtain a gun"

[bgset]
[effect type = 0 time = 500]

;//短くなった煙草を灰皿でもみ消す。
I butted out my cigarette.

【Reiji】
[v file="rei0077"]
;//「確か今年中に米軍基地の一部が返還されるだろう？　そうすりゃ其処で働く人間は大幅に削られる。金は有った方がいいだろうさ」
"If I'm not mistaken a part of that facility is going to be dissolved this year. That means some people working there will lose their job and will need money."

;//魚住が角張った顎に手を当てる。
Uozumi scratched his chin.

[bgset]
[charload num = 0 file = "kyo0207a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0015"]
;//「いや、しかし-証拠がない」
"We still don't have any proof"

【Reiji】
[v file="rei0078"]
;//「井の頭公園の池を浚ってみな。己の予想が正しけりゃいいモンが出てくるぜ」
"We still haven't searched the Inokashira park. I have a feeling, that if we look over the place, we'll definitely find something"

[bgset]
[charload num = 0 file = "kyo0101a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0016"]
;//「-わかった。鑑識に伝える」
"Alright. I'll report forensics"

【Reiji】
[v file="rei0079"]
;//「そのくらい警察で捜してくれ。己が出来る事は此処までだ」
"Since there's nothing more I can help with I'll leave that to police"

[bgset]
[charload num = 0 file = "kyo0107a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0017"]
;//「-わかった、こちらで調べさせる」
"Alright. I'll give an order"

[bgset]
[effect type = 0 time = 500]

;//魚住は机の上にある電話の元へ向かった。
He borrowed the telephone on my desk.

;//行動が早いのは賞賛すべき魚住の長所ではあるが、他人ン処の電話を勝手に使わないで欲しい。電話代だって莫迦にならないのに。
I always admired him for being able to act quickly in any situation. But I'd prefer, if he didn't use other people's phones without asking. Phone bills are really expensive nowadays.

[bgmin time=2000 file="bgm10a"]

【Uozumi】
[v file="uoz0018"]
;//「-おう玲人、もう一件仕事を受ける気はねえか？　宗教絡みの詐欺師の調査なんだが-」
"Hey, Reiji. Are you going to investigate another case? Some religious scam"

;//受話器を手で塞ぎながら魚住が訊ねてくる。
He asked after covering the phone with his hand.

【Reiji】
[v file="rei0080"]
;//「そういうのは上野の探偵に回してやれよ。危険が少ない仕事を探しているようだったからな」
"Let's leave this to our detective from Ueno. He was looking for a job that'd be a little easier on the bones"

;//知り合いの探偵の事を思い出し、そう魚住に告げる。
I said when remembered one guy I'm acquainted with.

[bgset]
[charload num = 0 file = "kyo0107a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0019"]
;//「所帯を持ってから守りに入ったのか？」
"Looking for safe jobs after making a family, huh?"

;//魚住が受話器を置き、こちらに向き直った。
Asked Uozumi after hanging up.

【Reiji】
[v file="rei0081"]
;//「嫁さんが妊娠しているらしいからな。心配かけたくないんだろ」
"His wife is pregnant, and it seems he doesn't want to make her worry"

[bgset]
[effect type = 0 time = 500]

;//危険な仕事は、独り身がやるもんだ。
Dangerous jobs are best suited for singles.

【Reiji】
[v file="rei0082"]
;//「-で、晩飯はどうする？　月世界にでも食いに行くか？」
"Wanna eat? Let go to the Moon World"

;//そう言った途端、魚住が盛大に咳き込んだ。
The moment I said that Uozumi coughed furiously.

[bgset]
[charload num = 0 file = "kyo0106a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0020"]
;//「てっ、テメエっ！！　何でわざわざ吉祥寺まで行かねえとならねえんだ！？」
"H-Hey!! Why should I go somewhere as far away as Kichijoji!?"

【Reiji】
[v file="rei0083"]
;//「じゃあいいぞ、己ひとりで行く」
"Fine then. I'll go by myself"

[bgset]
[effect type = 0 time = 500]

;//からかうように口端を歪め、魚住を見遣る。
Teasing Uozumi I looked at him intently.

[bgset]
[charload num = 0 file = "kyo0207a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0021"]
;//「い、いや……俺も行く。げっ、現場に行くついでだからな！！」
"N-no... I'll go, too. That's right, will go and see the crime scene as well"

;//貌を真っ赤にして怒鳴る魚住。解り易い奴だ。
He barked back, blushing. Uozumi can't tell lies.

【Reiji】
[v file="rei0084"]
;//「そうだな。杏子の飯は旨いからな」
"I just remembered, Kyoko is really good at cooking"

[bgset]
[charload num = 0 file = "kyo0106a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0022"]
;//「かっ-関係ねえだろ！！」
"H-how that's relevant!?"

;//唾を飛ばすな。
Don't splutter, when you're talking!

[bgset]
[effect type = 0 time = 500]

;//まあとにかく-腹減った。
In anyway, I'm hungry and gonna have some lunch.

【Reiji】
[v file="rei0085"]
;//「飯でも食いに行くか。もちろんお前の奢りで」
"We should probably have a bite. You're paying, of course"

[bgset]
[charload num = 0 file = "kyo0106a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0023"]
;//「ひとりで行ってこい。俺は帰る」
"Go by yourself. I'm going back to MPD"

;//魚住はにべも無く己の誘いを断ってきた。
Uozumi flatly refused my request.

[bgset]
[charload num = 0 file = "kyo0102a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Uozumi】
[v file="uoz0024"]
;//「まだ調べないとならねえ事が多いからな」
"There's too many things to be done"

[bgset]
[effect type = 0 time = 500]

;//そう言い残し、魚住は出て行った。
Saying that, he left the office.

;//仕方ない、ひとりで行くか-
Well, it seems I have no choice here.

[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0200_0000.ks"]
[end]
