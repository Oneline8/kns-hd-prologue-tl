;//朝一番で井の頭公園に呼び出された魚住は不機嫌さを隠そうともせず、血走った目で周囲を睥睨していた。
After arrival at Inokashira park Uozumi, who didn't even try to hide being in a foul mood, scanned the surroundings with his bloodshot eyes. 

;//多分に眠かったのもあるが、その所為で魚住の周りには誰も寄り付かなかった。
Other people in this area seemed to be sleepy as well, maybe that's why nobody came to greet him.

【Uozumi】
[v file="uoz0025"]
;//「初動措置は済んでるんだろうな？」
"Are you done with preparations?"

;//近くにいた警官の首根っこを引っ掴む。
Asked Uozumi after stopping an officer passing nearby.

【Officer】
[v file="kea0001"]
;//「は、はいっ！　鑑識課の方も到着しています！」
"Sir! Yes, sir! Forensics have already arrived, too"

【Uozumi】
[v file="uoz0026"]
;//「そうかい」
"I see"

;//警官を解放し、魚住はロープで区切られた現場へ足を踏み入れた。
Leaving that officer behind, Uozumi crossed the crime-scene rope.

[bgload file="bg55k.png"]
[effect type = 0 time = 1000]

;//仕事柄、魚住は死体を見慣れていた。
Of course, he was used to facing death at this job. 

;//だから目の前にあるそれを見た時も、さほど驚く事は無かった。
That's why seeing a corpse couldn't surprise him much.

;//とはいえ、ホトケをずっと衆目に晒しておく訳にもいかぬ。
But there was no way, they could let other people see it.

【Uozumi】
[v file="uoz0027"]
;//「鑑識は何処だ？」
"Where's the forensics member?"

;//さっさと現場検証をするに限る。
The sooner he's done with it, the better.

【Forensics Member】
[v file="kan0001"]
;//「こちらです魚住巡査部長。捜査の準備は整っています」
"Over here, sergeant Uozumi! We are ready to inspect the body"

【Uozumi】
[v file="uoz0028"]
;//「おう。気になる処を調べていくぞ」
"Great. Let's get started"

