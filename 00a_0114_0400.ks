[bgload file="bg08b.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm02a"]

;//新宿駅から国鉄に乗って西荻窪で降り、善福寺川に沿って北へ歩いていくと己達の家がある。
After arrival at Nishi-Ogikubo Station, we headed to the north alongside Zenpukuji river and came home.

;//数年前に両親が飛行機事故で死に、それからは己と紫だけがこの家の住人だ。
Since our parents died in an aircrash several years ago, we became the only inhabitants of this house.

;//尤も己が此処に居る事はあまりないのだが。
Although I'm hardly ever in here.

;//紫が扉の鍵を開け、中に入っていく。
Yukari opened the door and we went inside.

[bgload file="bg09b.png"]
[effect type = 0 time = 1000]

[bgset]
[charload num = 0 file = "yuk0101b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0074"]
;//「お洗濯物は脱衣所に置いてくださいね」
"Please, take the laundry to the changing room"

【Reiji】
[v file="rei0009"]
;//「その前に着替えてくる」
"Sure, let me just change"

[bgset]
[effect type = 0 time = 500]

;//風呂敷包みを居間に置き、襖を開けて隣の部屋に移動する。
Leaving the laundry at the living room, I went to another.

[bgload file="bg11b.png"]
[effect type = 0 time = 1000]

;//流石に日中誰も家に居なかった所為か、室内でも肌寒い。
It's pretty cold in here, because during day time there usually nobody at home. 

;//手早く着流しを羽織り、脱いだ服を携えて居間に戻る。
I quickly got changed, took my street clothes and returned to the living room.

[bgload file="bg09b.png"]
[effect type = 0 time = 1000]

[bgset]
[charload num = 0 file = "yuk0101b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0075"]
;//「夕飯は鶏鍋でいいかしら」
"How do you feel about torinabe?"

【Reiji】
[v file="rei0010"]
;//「ああ、任せる」
"Fine, sounds great"

[bgset]
[effect type = 0 time = 500]

;//簡潔に答え、己は風呂場へ向かおうと紫の横を通り過ぎようとした。
After giving a short answer, I try to get past Yukari to the bathroom.

[bgset]
[charload num = 0 file = "yuk0408b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0076"]
;//「お風呂に入りたいのでしたら、ご自分で沸かしてくださいね」
"If you're going to take a bath, heat it up yourself, okay?"

【Reiji】
[v file="rei0011"]
;//「……わかってるよ」
"I know"

[bgset]
[effect type = 0 time = 500]

;//着流しの合わせ目をしっかりと直して寒さに肩を竦めながら、己は瓦斯ボイラが湯を吐き出すのを凝と待つ事にした。
Shivering from cold I tightened the belt of my kimono and froze in anticipation of the boiler to heat up.

[bgload file="bg11c.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm02b"]

;//風呂を浴びて身体も暖まり、己は自室で読書の時間を過ごしていた。
After a warming soak in the bath, I returned to my room and picked up a book.

;//ガリア戦記をぱらぱらと捲りながら古代ローマに思いを馳せる。
Skimming through the history of Gallian wars, I was thinking about Ancient Rome.

;//幾度となく読んだ本ではあるが、読み返す度に新たな発見があるように思える。
I read this book so many times, and each time discovered something new.

[bgset]
[charload num = 0 file = "yuk0601a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0077"]
;//「夕飯が出来ましたよ」
"Dinner's ready"

;//着物に着替えた紫が己を呼びに来た。
Yukari, now wearing yukata, called me out.

[bgload file="bg09c.png"]
[effect type = 0 time = 1000]

;//土鍋から湯気と共に良い香りが漂ってくる。
From pots set on the table came the steam, spreading a pleasant aroma.

;//鶏肉に長葱、白菜春菊しらたき。椎茸が入っていないのが少々心残りだ。
Chicken with onion and noodles made from konnyaku. I wish there were some mushrooms in it.

;//代わりに茹で卵がふたつ、出し汁に浮かんでいた。
Instead there were two boiled eggs, floating in the soup.

[bgset]
[charload num = 0 file = "yuk0307a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0078"]
;//「さあ、いただきましょう」
"Well, let's eat"

[bgset]
[effect type = 0 time = 500]

;//紫が手を合わせ、黙礼する。
Yukari folded her arms and bowed.

;//基督教系の学院に通っている割に、こう云う処は純日本風なんだよな。
Although she goes to school with christian teachings, this trait of her is purely japanese.

;//小皿に注がれたポン酢醤油で鶏肉をつつく。
I eat chicken seasoned with ponzu from a small plate.

;//胃袋の中から身体が暖められていくような感覚。
A pleasant warm is spreading from stomach over the whole body.

【Reiji】
[v file="rei0012"]
;//「-熱燗でも一杯飲りたい気分だな」
"I could really use some hot sake right about now..."

[bgset]
[charload num = 0 file = "yuk0601a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0079"]
;//「そんな事を言っても、我が家にお酒はありませんよ」
"But we don't have any alcohol in our house"

;//澄まし顔で紫が言う。
Stated Yukari with a straight face

【Reiji】
[v file="rei0013"]
;//「気分だ、ってだけさ」
"Well, it was just a fad"

[bgset]
[effect type = 0 time = 500]

;//しらたきを啜りながら言い返す。
I replied sipping noodles.

【Reiji】
[v file="rei0014"]
;//「-ご馳走さん。美味かったよ」
"Thanks for dinner. It was delicious"

[bgset]
[charload num = 0 file = "yuk0302a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0080"]
;//「お粗末様でした」
"My pleasure"

[bgload file="bg11c.png"]
[effect type = 0 time = 1000]

;//自室に戻って煙草を燻らせながら、事務所から持ち帰った書類を捲る。
I returned to my room, lit a cigarette and immersed myself in the documents I brought from work.

;//創世記の第四章でカインがアベルを殺して以来、殺人と嘘はいつも共にある。
Since Cain killed Abel in Genesis 4, murder and lying always go together.

;//今回の事件も、まさに創世記の事件そのままに兄が弟を殺すと云うものだった。
Just like in the Bible, in this case elder brother killed his younger.

;//親の遺産を巡る諍いは富豪と呼ばれるような家では多々ある事だ。
Struggles over the inheritance is a common thing in rich families.

;//生憎時坂家は富裕層でも何でもなかったので、親の遺産と呼べる物はこの家と大量の蔵書くらいだった。
Unfortunately, our family wasn't rich, and the only things we inherited from our parents were this house and a large collection of books.

【Reiji】
[v file="rei0015"]
;//「-御陰様でうちは家族円満だ」
"That's why we're a happy family"

[bgset]
[charload num = 0 file = "yuk0307a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0081"]
;//「-そうですね」
"Sure we are"

;//部屋に入ってきた紫が頷く。
Bowed Yukari when entered the room.

[bgset]
[charload num = 0 file = "yuk0301a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0082"]
;//「そろそろ寝ます」
"I'll go get ready for bed"

【Reiji】
[v file="rei0016"]
;//「ああ、お休み」
"Okay, good night"

[bgset]
[charload num = 0 file = "yuk0604a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0083"]
;//「兄さんも夜更かしなどせずに早く眠ってくださいね」
"Don't stay up too late, okay?"

【Reiji】
[v file="rei0017"]
;//「……子供じゃないんだから」
"I'm not a child, you know..."

[bgset]
[charload num = 0 file = "yuk0602a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0084"]
;//「ふふっ……おやすみなさい」
"Hehe... Have a good night"

[bgset]
[effect type = 0 time = 500]

[se loop=0 file="se014"]

;//紫が襖を閉めて出て行った。
And Yukari left.

;//短くなった煙草を灰皿に押しつける。
I put my cigarette into ashtray.

【Reiji】
[v file="rei0018"]
;//「……寝るか」
"Maybe I should go to bed, too"

;//立ち上がって押入から布団を出す。
I took the futon from shelf.

[bgload file="bg11d.png"]
[effect type = 0 time = 1000]

【Reiji】
[v file="rei0019"]
;//「ふう……」
"..."

;//毛布にくるまり、息を吐く。
Sighing I wrap myself inside it.

;//明日は捜査した事件の顛末を警察に報せて-
Tomorrow I'll report the investigation details to police.

【Reiji】
[v file="rei0020"]
;//「ああ-明日は日曜か……」
"Ah, tomorrow is Sunday..."

;//まあいい。どうせ奴は出勤しているだろう。
Nevermind, this guy will be at work anyways.

;//貧乏暇なしはお互い様だ、探偵も-警官も。
Poor people have no time to rest. That's our common ground.

;//己は布団の冷たさに行火を入れておけば良かったと後悔しつつ眠りに就いた。
I regretted, that I hadn't prepared the heating pad, but fell asleep anyways.

[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0115_0100.ks"]
[end]
