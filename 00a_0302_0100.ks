[bgload file="cutin01.png"]
[effect type = 0 time = 1000]

[bgload file="bg56e.png"]
[effect type = 0 time = 1000]

;//夜明け前に電話で叩き起こされた魚住は、昨年配備されたばかりのトヨペットのパトカーに乗って府中の多磨霊園までやってきた。
Before the sun came up Uozumi who got woke up by a phone call arrived to Tama cemetery on his Toyopet patrol car, which was produced last year.

;//濃い霧が墓場を覆い隠していた。魚住は枯枝を踏み散らしながら奥を目指す。
The graveyard is covered by a thick fog. Stepping on branches scattered all over the place, Uozumi goes deep into the place.

【Uozumi】
[v file="uoz0056"]
;//「-此処か」
"----This is it?"

;//警官が何人も集まっていた。
A group of officers are assembled there.

【Officer】
[v file="kea0004"]
;//「お疲れ様です」
"Thanks for coming out here"

【Uozumi】
[v file="uoz0057"]
;//「おう。どうなってんだ？」
"Sure. What's the situation?"

;//敬礼をしてきた警官に尋ねる。
He addresses the officer who had saluted him.

【Officer】
[v file="kea0005"]
;//「はっ。第一発見者は付近の住人です。昨晩遅くに此処で何かが燃やされているのを発見したとの事です」
"Sir! Late last night, a local resident noticed a fire in the area. He came to investigate and found this"

【Uozumi】
[v file="uoz0058"]
;//「で、ホトケは何処だ？」
"All right. Where's the corpse?"

【Officer】
[v file="kea0006"]
;//「-こちらです」
"Please, come this way"

;//警官が先導して歩き出す。
Uozumi followed the officer.

;//現場を厭な緊張感が支配しているのを魚住は感じていた。
Suddenly he felt tension in the air.

;//ただの焼死体であるならば、こうはならないだろう。
Seemed, that it wasn't just a burnt corpse.

;//最近は物騒な事件が多すぎる。
Too many of these disturbing cases have cropped up lately.

;//先日のバラバラとて未だ手懸かりが掴めていないと云うのに-
And he still hasn't found any leads on the dismemberment case from the other day.

【Uozumi】
[v file="uoz0059"]
;//「-厭な感じだ」
"I have a bad feeling about this"

;//鼻腔を擽る不快な臭いに魚住は吐き捨てた。
The odor tickling his nose made Uozumi spat out.

;//それは-戦地で嗅いだ臭いだ。
That smell reminded him of a battlefield.

;//焼夷弾で灼き尽くされた村に漂う臭いだ。
Smell of the villages after bombing.

;//戦死した同胞の遺体を焼いた時の臭いだ。
Smell of the burned corpses of his dead comrades.

;//炭火で焼いた肉とは全く違う、機械油の染みついた不快な臭いだ。
It was mixed not with coal smell, but an awful smell of machine oil.

【Officer】
[v file="kea0007"]
;//「-どうぞ、此処です」
"Over here, Sir"

;//警官の声で魚住は我に返り、彼の指し示す処を見た。
A voice of the officer brought Uozumi back to reality and he saw that----

【Uozumi】
[v file="uoz0060"]
;//「……なんだってんだ、こいつは……」
"What the hell..."

[bgload file="black.png"]
[effect type = 0 time = 1000]

[bgload file="te005.png"]
[effect type = 0 time = 1000]

[bgload file="te005_.png"]
[effect type = 0 time = 1000]

;//訳が判らなかった。
Unbelievable.

;//その光景は、現実主義者の魚住には全く不可解だった。
Such scene was absolutely mysterious for some realist like Uozumi.

[bgload file="bg76d.png"]
[effect type = 0 time = 1000]

;//本当に-最近は事件が多過ぎる。
Right---- lately too many of disturbing cases have cropped up.

;//身ひとつではとても間に合わない。
It's too much for him to handle on his own.

【Uozumi】
[v file="uoz0061"]
;//「……奴に任せるか」
"...Should I bring him in on this?"

;//こういった事件にばかり首を突っ込んでくる探偵を思い出す。
A certain detective comes to mind; one who's always sticking his nose into cases like this.

;//まさにあいつ-時坂玲人向きの事件だった。
Tokisaka Reiji----this case is right up his alley.

[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "01a_0303_0000.ks"]
[end]
