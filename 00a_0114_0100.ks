[bgload file="bg20a.png"]
[effect type = 0 time = 1000]

;//しんと静まりかえった講堂。
It was quiet in the audience.

;//ぴんと張り詰めた空気。
There was a lot of tension in the air.

;//一心に祈りを捧げる少女たち
In the middle of the room girls were praying intently.

;//講堂を満たす重圧は、決して沈黙の所為だけではない。
Not only silence was the reason for tension.

;//彼女たちの前には大きな十字架。
Over the girls towered a huge cross.

;//礼拝堂の役割も果たす、私立櫻羽女学院の講堂。
This place, known as Chapel, was a part of Ouba Private Girls Academy.

;//一月の冷たい空気すらも、屋内ではさほど気にならない。
Even January's cold air didn't bother too much.

;//この場は更に冷たい空気で支配されているのだから-
It's because the atmosphere of this place was much colder itself.

;//時坂紫はそっと薄目を開け、周囲を見回した。
Tokisaka Yukari slowly glanced around.

;//誰もが目を閉じ、十字架に祈りを捧げている。
Everyone else had their eyes closed, staying in prayer.

;//隣の女学生と目が合った。
Then her eyes met with the girl, sitting nearby...

[bgset]
[charload num = 0 file = "toj0102a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Student】
[v file="toj0001"]
;//「退屈だね」
"This is boring"

【Yukari】
[v file="yuk0001"]
;//「……そうね」
"...Yeah"

[bgset]
[charload num = 0 file = "toj0102a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//彼女は四十宮綴子。紫の級友で数少ない親友のひとりだ。
This girl, Yosomiya Tsuzuriko, was Yukari's classmate and one of her few friends.

[bgset]
[charload num = 0 file = "toj0104a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Tsuzuriko】
[v file="toj0002"]
;//「はーぁ、寒ぅ……」
"Ahh, and I'm freezing..."

【Teacher】
[v file="sae0001"]
;//「そこ、静かになさい」
"Be quiet!"

[bgset]
[charload num = 0 file = "toj0306a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//綴子の声を聞き咎めた教頭が静かな声で注意をする。
Said the teacher who heard her talking.

[bgset]
[charload num = 0 file = "toj0311a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//綴子は素直に頭を垂れた後、紫に向かって小さく舌を出して戯けてみせた。
At first Tsuzuriko obediently bowed her head, but then for a short moment stuck out her tongue.

[bgset]
[effect type = 0 time = 500]

【Student】
[v file="ori0001"]
;//「-戦争より十年が経過し、また新しい年を迎えました。私たちは過去の反省をふまえ、これから我が国の将来を背負って往かねばなりません」
"It been 10 years since war ended. Another year passed in peace. We must continue taking care of the future of our country----"

;//会長の澄んだ声が講堂に響く。
The clear voice of a student body president broke the silence in the audience.

;//礼拝は終わり、新学期の始業式に移っていた。
The prayer has ended and the opening ceremony of the new academic semester began.

;//会長の月島織姫は澄んだ綺麗な声で朗々と演説を続ける。
Tsukishima Orihime in a beautiful sonorous voice cheerfully continued to speak.

;//彼女を慕う女学生は多いと聞き及んでいる。
Many students adore her.

;//紫の隣に居る綴子もそのひとりだ。
Tsuzuriko is one of them.

;//ちらりと横目で見遣ると、彼女は確かにうっとりと織姫の言葉に聞き惚れているように思える。
With a quick glance, you can see how admirably girls were listening to the words of Orihime.

;//そういう嗜好もあるのだろう。紫にはよく解らないけれど。
This was something Yukari didn't quite understand

【Orihime】
[v file="ori0002"]
;//「-神の御心の侭に、我々も精進せねばなりません。皆様、清廉たる意志を持って過ごして参りましょう」
"Obeying the will of God, we must be cleansed. So let's take this path with righteous thoughts."

[se loop=0 file="se058"]

;//パチパチと多くの拍手が鳴り響く。つられて紫も形だけの拍手を送った。
With a loud applause, Yukari also formally folded her arms.

【Teacher】
[v file="kus0001"]
;//「松組から順に教室へ戻りなさい」
"Now starting with Matsumoto class, leave the audience and return to your classes."

;//教師の声に従い、講堂を出て行く。
Obeying teacher's words, students began to disperse.

[bgload file="bg14a.png"]
[effect type = 0 time = 1000]

;//教室へ戻ると、さっそく綴子が話し掛けてきた。
After they got to their class Tsuzuriko immediately spoke.

[bgmin time=2000 file="bgm05"]

[bgset]
[charload num = 0 file = "toj0302a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Tsuzuriko】
[v file="toj0003"]
;//「ねねっゆかりん、やっぱり織姫さまって凛としてて素敵だよねっ！？」
"Hey, Yukarin, Orihime is so beautiful, isn't she?"

[bgset]
[charload num = 0 file = "toj0302a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0002"]
;//「そ……そうね……」
"Uh... yeah..."

[bgset]
[charload num = 0 file = "toj0302a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//紫は綴子ほど織姫に心酔している訳ではなかったが、確かに彼女には他の人にない魅力があるように感じていた。
Yukari wasn't fond of Orihime as much as Tsuzuriko, but she did admit that there was something special about her.

[bgset]
[effect type = 0 time = 500]

;//英雄資質とでも云うのだろうか、人を惹き付ける能力は同年代の紫から見ても抜きん出ているように思える。
You can even call it charisma, or the ability to fascinate people, which strongly distinguishes her from the rest of students.

;//女学生のみの学院内で彼女に心酔する者が多いのも頷ける。
One could understand why she has many admirers in a girls academy----

;//彼女-綴子もそうなのだろう。
And why Tsuzuriko was one of them either.

;//そういった嗜好があるようには思えないけれどと、紫は綴子をちらりと見遣る。
Although she herself may not think so. Yukari looked at her.

[bgset]
[charload num = 0 file = "toj0106a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//四十宮綴子は文学少女だ。それも一般の文学雑誌などに小説を載せる程の才能を有している。
Yosomiya Tsuzuriko loves literature. In addition, she is talented herself and even writes short stories for literary magazines.

;//勿論学院には内証の事だ。厳しい校則故、文筆活動などもってのほかだった。
Of course, no one here knows about her hobby. Strict rules of the academy don't allow such activities.

;//しかも綴子は、普通の文芸誌のみならず、低俗誌-いわゆるカストリ雑誌にも原稿を寄せていた。
However she sends her stories not only to ordinary magazines, but even to low-grade, the so-called Kastori.

;//そちらの小説を紫は読んだことがない。読まなくていいと綴子に謂われたからだ。親友の厭がる事をするような趣味は紫にはない。
Yukari never read these stories, but Tsuzuriko wasn't bothered. Yukari, on the other hand, had no hobby that Tsuzuriko didn't like.

[bgset]
[charload num = 0 file = "toj0306a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Tsuzuriko】
[v file="toj0004"]
;//「……ゆかりんどうしたの？　あたしの貌じっと見ちゃって」
"What's wrong, Yukarin? Is there something on my face?"

【Yukari】
[v file="yuk0003"]
;//「あっ……ううん、何でもないわ」
"Ah... no, it's nothing."

[bgset]
[effect type = 0 time = 500]

;//首を振りながら紫は視線を綴子から離した。
Shaking her head, Yukari looked away.

【Yukari】
[v file="yuk0004"]
;//「ほら、日下先生が来たわよ」
"Hey, look. Kusaka-sensei is here."

;//この場をそう誤魔化しながら、紫は前を向いた。
She said, taking an opportunity.

【Teacher】
[v file="kus0002"]
;//「皆さん、おはようございます」
"Good morning, everyone"

;//担任教師の日下達彦が教室に入ってきた。
Their teacher, Tatsuhiko Kusaka, has entered the classroom.

[bgset]
[charload num = 0 file = "tat0102a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kusaka】
[v file="kus0003"]
;//「本日授業はありませんが、これから暫くの時間、学年主任の先生に学生としての過ごし方を指導して貰います」
"Today there will be no lesson, instead, the class teacher will instruct you on how to spend your time properly."

[bgset]
[effect type = 0 time = 500]

;//最近はことある毎に生活指導が行われているような気がする。
Recently, such rearrangements occur regularly.

;//それも仕方ないのかもしれない。
Apparently it should be so.

;//櫻羽女学院は名門と呼ばれている。明治の代よりこの武蔵野の地に門を構え、良家の子女を数多く輩出してきた。
Ouba Girls Academy was famous for it's history. It was founded during the Meiji period in Musashino. A lot of students from rich families graduated from it.

;//それ故に校則も厳しい。
Therefore, the rules were strict here.

;//外出時の制服着用は勿論の事、吉祥寺や新宿等の繁華街への保護者を伴わない立ち入りの禁止。
Mandatory uniforms, even outside. Don't walk alone in central areas like Shinjuku.

;//米軍関係者との接触禁止等、日常生活における些細な事すら規制されている。
Don't make contacts with American soldiers. In everyday life even minor things were controlled.

;//それはそれで構わない。紫はわざわざ校則に背くような真似はしない。ただこのように無駄な時間を取られるのが厭なだけだ。
Yukari didn't mind this. She'd never committed intentionally unlawful things. She didn't like wasting her time on this.

[bgmstop]

【Teacher】
[v file="tea0001"]
;//「聞いているのですか、朽木さん！」
"Are you listening, Kuchiki-san?"

;//神経質そうな教師の金切り声に紫は顔を上げた。
Yukari raised her head, when heard the loud cry of the teacher.

[bgset]
[charload num = 0 file = "huy0501a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Student】
[v file="kto0001"]
;//「-聞いています」
"I'm listening"

;//凛とした声。注意された女学生-朽木冬子だ。紫と同じ美術部に所属している。
A pleasant voice. The one who caught the attention of Yukari was Kuchiki Toko. They both joined the art club.

【Teacher】
[v file="tea0002"]
;//「こちらを向きなさい！」
"Look at me when I speak!"

[bgset]
[charload num = 0 file = "huy0505a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//彼女は窓の外を眺めていたらしい。横目で教壇に立つ教師を視ているようだ。
It seems she was looking out the window, and now she is watching the teacher standing in the pulpit with a side look.

[bgmin time=2000 file="bgm05"]

;//彼女-朽木冬子は風変わりな少女だった。
This girl was rather strange.

;//すらりと伸びた手足、きめ細やかな肌、絹のような長い黒髪。同性からもそれは羨ましく見えた。
Slender arms and legs, smooth skin, silky long hair. Many girls envied her.

【Teacher】
[v file="tea0003"]
;//「何ですかその目は……！　睨み付けたりなんかして！」
"Why are you looking at me like that!? What are you staring at!?"

[bgset]
[charload num = 0 file = "huy0104a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="kto0002"]
;//「-睨んでなどいません」
"I'm not staring"

;//冬子は飄々とした口調で抗弁した。事実睨み付けてなどいないのだろう。
Toko answered easily. Looks like she really isn't.

;//紫の位置から見える彼女の表情などたかが知れているが、少なくとも此処から窺える彼女の表情は落ち着き払っているようだった。
From Yukari's place, Toko's expression didn't hold any particular seriousness, in any case she remained calm.

;//まるで叱られているのを自覚していないかのように。
As if she wasn't scolded.

[bgset]
[effect type = 0 time = 500]

;//他の学生は誰しもが我関せずと云った様子で行儀良く席に着いている。それもまた、何処か異質だった。
Other students diligently sat in their seats.

【Teacher】
[v file="tea0004"]
;//「其処で立っていなさい！」
"Stand up, now!"

;//指示棒を冬子の胸元に突きつけ、教師は壇上へ戻った。
After poking Toko with a pointer teacher returned to the pulpit.

;//教室内にちらりと視線を走らせる。隣の綴子は退屈そうに頬杖をついている。その向こう、先程まですっくと立っていた冬子が俯いている。
Yukari looked around the class. Tsuzuriko sat bored, propping her cheek with her arm. Toko, who was just standing still, hunged her head.

【Yukari】
[v file="yuk0005"]
;//「朽木さん……？」
"Kuchiki-san?"

[bgset]
[charload num = 0 file = "huy0503a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//よろよろと冬子が頭をあげた。顔色が悪い。
Swaying, Toko raised her head. Her face turned pale.

;//目が合った一瞬の後。
Their eyes met and...

[bgmstop]

[se loop=0 file="se043"]

【Student】
[v file="mto0001"]
;//「冬子っ！？」
"Toko!?"

[bgload file="te001.png"]
[effect type = 0 time = 1000]

;//そばの席に座っていた女学生が倒れた冬子に駆け寄った。水原透子、冬子の親友だ。
The girl sitting nearby rushed to collapsed Toko. This girl was Mizuhara Toko, a friend of hers.

【Teacher】
[v file="tea0005"]
;//「白々しい真似を……放っておきなさい」
"Stop pretending!"

【Toko】
[v file="mto0002"]
;//「そんなっ……」
"She isn't!"

;//にべもない教師の物言いに、透子が声をあげる。
Shocked by the teacher's coldness Toko raised her voice.

【Toko】
[v file="mto0003"]
;//「冬子っ、冬子っ！」
"Toko! Toko!"

[bgmin time=2000 file="bgm05"]

【Toko】
[v file="kto0003"]
;//「私なら……大丈夫……」
"I'm... fine..."

【Toko】
[v file="mto0004"]
;//「でもっ……」
"But..."

;//端から見ていた紫は我慢できずに立ち上がった。
Yukari could no longer remain indifferent and stood up.

【Yukari】
[v file="yuk0006"]
;//「保健室へ連れて行きます」
"I'll take her to the infirmary"

【Teacher】
[v file="tea0006"]
;//「……貴女は？」
"Why?"

【Yukari】
[v file="yuk0007"]
;//「保健委員です」
"I help there"

;//紫は教師の訝しげな問いに答えると、冬子に歩み寄った。
Saying that, Yukari went to Toko.

【Yukari】
[v file="yuk0008"]
;//「朽木さん……」
"Kuchiki-san..."

【Toko】
[v file="kto0004"]
;//「ええ……」
"Yes..."

[bgload file="bg14a.png"]
[effect type = 0 time = 1000]

;//よろめきながらも冬子は立ち上がる。彼女の反対側の肩を透子が支えた。
Toko stood up, swaying. Mizuhara Toko supported her from an opposite side.

[bgset]
[charload num = 0 file = "tou0309a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="mto0005"]
;//「私もっ……」
"I will help, too..."

【Teacher】
[v file="tea0007"]
;//「いけません。水原さんは席に戻りなさい」
"No. Mizuhara-san, go back to your seat"

[bgset]
[charload num = 0 file = "tou0307a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="mto0006"]
;//「-っ」
"!"

【Yukari】
[v file="yuk0009"]
;//「……保健室へ行きます」
"We'll be going then"

[bgset]
[effect type = 0 time = 500]

;//紫は透子の視線を背中に感じながら、冬子に肩を貸して教室を出た。
Feeling Toko's gaze, Yukari gave her a support, and they left the classroom.

[bgload file="bg15a.png"]
[effect type = 0 time = 1000]

【Yukari】
[v file="yuk0010"]
;//「大丈夫ですか……？」
"Are you alright?"

;//ゆっくりと廊下を歩きながら紫は話し掛けた。
Asked Yukari, while they slowly passed the corridor.

[bgset]
[charload num = 0 file = "huy0105a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="kto0005"]
;//「うん……只の、貧血だろうから……」
"Yeah... They say it's anemia..."

;//紫の身体にもたれ掛かり、息を切らしながら冬子は呟く。
She whispered, panting and swaying.

[bgset]
[charload num = 0 file = "huy0503a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="kto0006"]
;//「だから、もう……戻っても大丈夫……」
"So, we shall go back"

【Yukari】
[v file="yuk0011"]
;//「そんな-」
"I don't think so"

[bgset]
[effect type = 0 time = 500]

;//とても大丈夫のようには見えなかった。
Toko didn't look like she was okay.

;//自分ひとりでは真っ直ぐ立つ事すらままならぬのに。
She couldn't even stand still without support.

【Yukari】
[v file="yuk0012"]
;//「……もうすぐ保健室ですから」
"We're almost there"

;//だから紫は、冬子の言葉を無視して保健室まで連れて行った。
That's why Yukari led her to the infirmary, ignoring all what she said.

[bgload file="bg18a.png"]
[effect type = 0 time = 1000]

;//冬子の身体をベッドに横たえる。
She put Toko to the bed.

[bgload file="bg18k1.png"]
[effect type = 0 time = 1000]

【Toko】
[v file="kto0007"]
;//「はあっ……」
"Haa..."

;//大きく息を吐いて彼女は瞼を閉じた。
Toko heaved a sigh and closed her eyes.

【Yukari】
[v file="yuk0013"]
;//「今保健の先生が来ると思うから」
"Nurse will be back soon"

;//布団を直しつつ声を掛ける。
Said Yukari after fixing her blanket.

【Toko】
[v file="kto0008"]
;//「ええ-」
"Thanks"

;//頷く冬子の額に手を当てる。熱はないようだ。
Yukari put her hand on Toko's forehead. Her temperature was normal.

[se loop=0 file="se011"]

[bgset]
[charload num = 0 file = "nen0202a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【???】
[v file="nen0001"]
;//「朽木さん、具合はどう？」
"How do you feel, Kuchiki-san?"

;//扉を開けて養護教諭の朱崎寧々が入ってきた。
The nurse, Akazaki Nene, entered the room.

【Toko】
[v file="kto0009"]
;//「だいぶ……良くなりました」
"I'm better now"

[bgset]
[charload num = 0 file = "nen0202a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//布団の中から冬子が頷く。
Answered Toko, still in her bed.

【Yukari】
[v file="yuk0014"]
;//「では、私はこれで」
"Well then, I should go back"

[bgset]
[effect type = 0 time = 500]

;//一礼して保健室を辞そうとした紫に朱崎が声を掛けてきた。
Yukari bowed her head, ready to leave.

[bgset]
[charload num = 0 file = "nen0402a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Nene】
[v file="nen0002"]
;//「時坂さん、もうすぐ放課後になるから此処に居ても構わないわよ」
Tokisaka-san, lessons will be over very soon. You should stay here.

【Yukari】
[v file="yuk0015"]
;//「ですけど……」
"But I..."

[bgset]
[charload num = 0 file = "nen0209a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Nene】
[v file="nen0003"]
;//「朽木さんの傍に居てあげてちょうだい」
"Please, stay a bit more with Kuchiki-san"

[bgset]
[effect type = 0 time = 500]

;//そう謂われ、紫はちらりと冬子を見た。
Yukari moved her gaze to Toko, who quietly watched them talking. 

;//凝と彼女は紫を見つめている。不安気な瞳が微かに揺れているように紫には思えた。
For a moment Yukari believed, that her eye twitched a little.

【Yukari】
[v file="yuk0016"]
;//「……判りました」
"Okay"

;//頷いて丸椅子に腰を下ろす。
She gave a small bow and sat in the chair.

【Toko】
[v file="kto0010"]
;//「御免なさい」
"Sorry for that"

【Yukari】
[v file="yuk0017"]
;//「ううん、いいのよ」
"Nevermind"

;//謝ってくる冬子に紫は微笑みかけた。
She smiled.

;//誰だって体調を崩している時は不安になるものだ。
Anyone in Toko's situation would feel uneasy.

;//紫もあまり健康的とは云えないから、その気持ちは良く解る。熱を出して寝込んだときなどは、兄の存在がとても心強かったのを覚えている。
Yukari couldn't call healthy even herself, so she knew that feeling. That moment she remembered, how her brother stayed beside her, when she had a fever.

[bgmstop]

[se loop=0 file="se017"]

[bgload file="bg18k1.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm05"]

[bgset]
[charload num = 0 file = "nen0201a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Nene】
[v file="nen0004"]
;//「終わったみたいね」
"Looks like classes got out"

;//机に向かっていた朱崎が呟いた。
Said Nene.

[bgset]
[effect type = 0 time = 500]

;//冬子の様子を窺う。
She checked Toko's condition.

;//瞼を閉じ、呼吸も落ち着いているようだ。
Her eyes were closed, and breath was calm.

;//もう大丈夫だろうと、紫がそっと席を立った時だった。
She looked much better. Yukari stood up without making a noise and prepared to leave, when----

[se loop=0 file="se011"]

[bgset]
[charload num = 0 file = "tou0106a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="mto0007"]
;//「冬子っ……！」
"Toko!"

[bgset]
[effect type = 0 time = 500]

;//保健室の扉を開け、透子が飛びこんできた。
Another girl, whose name also was Toko, broke into the room.

【Yukari】
[v file="yuk0018"]
;//「水原さん……」
"Mizuhara-san..."

;//予鈴が鳴ってすぐこちらへ来たのだろう、息を切らしながら冬子の傍にやってくる。
She was out of breath. Looks like she ran over here right after the bell.

[bgset]
[charload num = 0 file = "tou0301a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="mto0008"]
;//「大丈夫、冬子……？」
"How do you feel, Toko?"

【Toko】
[v file="kto0011"]
;//「ええ-」
"I'm fine"

[bgset]
[charload num = 0 file = "tou0301a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//薄目を開けた冬子が答える。
She replied and opened her eyes.

【Toko】
[v file="kto0012"]
;//「……朝食を抜いたのがいけなかったのかな」
"I shouldn't have skipped my breakfast"

[bgset]
[effect type = 0 time = 500]

;//冗談めかして言う冬子に、透子がきつい視線を向けた。
But other Toko didn't like that joke.

[bgset]
[charload num = 0 file = "tou0105a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="mto0009"]
;//「本当に……心配したんだからっ……」
"I was so worried about you..."

[bgset]
[charload num = 0 file = "tou0105a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

[bgset]
[charload num = 0 file = "nen0403a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Nene】
[v file="nen0005"]
;//「ちゃんと食べなきゃ駄目よ」
"You should eat properly"

[bgset]
[effect type = 0 time = 500]

;//苦笑しながら朱崎は冬子を窘める。
Said Nene with a bitter smile.

【Toko】
[v file="kto0013"]
;//「そうですね」
"You're right"

;//透子は心から冬子の事を心配しているようだ。
Looks like another Toko was really worried.

;//やはり名前の響きが同じだからだろうかと紫は考えてみる。
'Could it be that's because their names sound similar' thought Yukari.

;//透子も紫と同じ美術部だったが、教室でも部活動でもあまり会話を交わした事はない。
Mizuhara Toko was in the art club, too, but they never talked much neither there nor in class.

;//お互い大人しい、目立たない存在だからだろう。
Mutually calm, not very prominent.

;//その点、冬子は目立つ存在とも云えた。
Kuchiki Toko on the other hand stood out. 

;//物静かな存在と云う点では紫とさほど違わない。
Well, both her and Yukari were the same calm.

;//だが朽木冬子と云う存在には華があった。
But also Toko was very beautiful.

;//穢れなく凛と咲き誇る、清らかなる一輪の白き花。綴子はそう朽木冬子を評していた。
Pure and majestic beauty, like a white flower. That's how Tsuzuriko described her.

;//紫にもなんとなく解る気がする。冬子はまさに一輪の花-他の者を寄せ付けない雰囲気を発している。
Yukari could imagine that. The white flower, surrounded by the atmosphere of loneliness.

;//一緒にいるのは、この透子だけだ。……まあ紫も、いつも共にいるのは綴子くらいなのだけれど。
She had no friends besides Mizuhara Toko. Just like Yukari had no friends besides Tsuzuriko.

;//その透子が紫にきつい視線を投げかけてきた。
And now that Toko was looking at her strictly.

[bgset]
[charload num = 0 file = "tou0307a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Toko】
[v file="mto0010"]
;//「時坂さん、冬子には私が付いているからもういいわよ」
"Tokisaka-san, I'll take care of Toko"

【Yukari】
[v file="yuk0019"]
;//「ええ……」
"Alright..."

[bgset]
[effect type = 0 time = 500]

;//透子はそれまで紫が腰掛けていた椅子に座り、冬子と指を絡め合っている。
That Toko was sitting in the chair, where Yukari sat before, and holded Kuchiki Toko's hand.

【Yukari】
[v file="yuk0020"]
;//「それでは、私はこれで」
"I'll be going then"

[se loop=0 file="se012"]

[bgload file="bg15a.png"]
[effect type = 0 time = 1000]

;//紫が廊下に出ると、其処には綴子が待っていた。
Tsuzuriko was waiting for her in the corridor.

[bgset]
[charload num = 0 file = "toj0302a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Tsuzuriko】
[v file="toj0005"]
;//「はい、ゆかりんの鞄」
"Here, I brought your bag"

【Yukari】
[v file="yuk0021"]
;//「あ……ありがとう」
"Ah... Thanks"

[bgset]
[charload num = 0 file = "toj0302a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//屈託のない笑顔の綴子から鞄を受け取る。
Tsuzuriko gave Yukari her bag, smiling.

[bgset]
[charload num = 0 file = "toj0104a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Tsuzuriko】
[v file="toj0006"]
;//「元気なさそうだけどどうしたの？」
"Are you alright? You seem a little down"

【Yukari】
[v file="yuk0022"]
;//「え……そう？」
"Really?"

[bgset]
[charload num = 0 file = "toj0104a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//気分が沈んでいたのだろうか、紫は頬に手を当ててみるが、それで自らの顔色は判る筈がない。
Maybe her mood has taken sour turn. Yukari touched her cheeks, but couldn't find out what's wrong.

[bgset]
[charload num = 0 file = "toj0305a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Tsuzuriko】
[v file="toj0007"]
;//「-気の所為だったかな」
"Well, I must've imagined it then."

[bgset]
[effect type = 0 time = 500]

;//苦笑いを浮かべながら綴子は頬を掻いた。
Tsuzuriko scratched her head. She must've been worried about Yukari.

;//紫を気遣ってくれているのだろう。綴子のそう云う処が紫は好きだった。
Yukari always liked that quality of hers.

【Yukari】
[v file="yuk0023"]
;//「……ありがとう、綴子」
"Thanks, Tsuzuriko"

;//だから紫は聞こえるか聞こえないか程の小声で綴子に礼を述べた。
That's why she thanked her almost soundlessly.


[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0114_0110.ks"]
[end]
