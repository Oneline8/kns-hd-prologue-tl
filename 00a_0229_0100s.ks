;//水際に死体が遺棄されていた。
There's a body on the lakeside.

;//此処からではよく判らない。
Can't see anything from here.

;//もう少し近付いて調べる必要があるだろう。
I should get closer to inspect the body.

【Uozumi】
[v file="uoz0029"]
;//「……現場は井の頭公園のど真ん中か」
"...Right in the center of Inokashira park, huh..."

;//魚住は周囲を見回して呟く。
Mumbled Uozumi scanning the surroundings

;//公園のほぼ中央とは云え、草木に覆われていて辺りからは死角になっている。
Although it's in the heart of the park, this place is hidden by trees and can't be spotted easily.

;//これ以上は、もっと近付かないと判らない。
I won't be able to get more information until I get closer.

;//池の周辺には立ち入り禁止のロープが張られている。
The area around the pond is restricted with the crime-scene rope.

;//当然ながら関係者以外立ち入り禁止だ。
Of course, we don't need unauthorized persons in here.

;//特に変わった所はない。
Nothing interesting.

【Uozumi】
[v file="uoz0030"]
;//「おう、この布切れはなんだ？」
"Tell me about this piece of cloth"

;//遺体の腕に絡み付いていた黒い布を指して訊ねる。
Said Uozumi pointing at the black cloth wrapped around corpses arms.

【Forensics Member】
[v file="kan0002"]
;//「遺体を包んでいた布のようです。妙な事に両腕だけがこいつにくるまれていました」
"Seems to be a simple cloth used for wrapping dead bodies. I wonder, why the criminal used it only for arms"

;//詳しい事は鑑識に廻してからでないと判らないだろう。
I need to look for other clues in order to form a proper opinion.

【Uozumi】
[v file="uoz0031"]
;//「腕-だな」
"So this is her arms, huh?"

;//相当腐敗が進んでいるようだ。
Looks like they're already half decomposed.

;//悪臭が漂ってくる。
There's an awful stench in the air.

【Forensics Member】
[v file="kan0003"]
;//「腕だけがこの黒い布に包まれていたようです」
"It seems the black cloth is only used for arms"

【Uozumi】
[v file="uoz0032"]
;//「脚は-付け根から切られているのか」
"Her legs... were cut off at the joint level"

;//無造作に二本の脚が地面に転がされていた。
Both were carelessly lying on earth.

;//腐った部分に蛆が湧いている。
Rotten parts were already full of maggots.

【Uozumi】
[v file="uoz0033"]
;//「やれやれ、堪らねえな-」
"That's disgusting"

【Forensics Member】
[v file="kan0004"]
;//「脚は発見時から剥き出しだったそうです」
"Since the moment we discovered the body, her legs hadn't been covered by anything"

【Uozumi】
[v file="uoz0036"]
;//「別嬪さんが台無しだな」
"We've lost such a beautiful girl"

;//頭部は腐敗が進んでいるものの、辛うじて女だと解る状態だった。
Even with this degree of decomposition her head still could be recognized as a female's.

;//眼窩は落ち窪み、白い蛆が顔中の穴を蝕んでいた。
Her eyes caved in, and white maggots were crawling all over her face.

;//布を捲った途端に腐臭が鼻をついた。
The moment I pushed the cloth, a stench of rotten flesh hit my nose

【Uozumi】
[v file="uoz0037"]
;//「蛆まで湧いてやがる……死んでから随分と時間が経っているようだな」
"There're even maggots in here... It's been a while since she died"

【Forensics Member】
[v file="kan0006"]
;//「蛆の成長具合からみて……この時期だと二週間近く経っているようですね」
"Judging by their size... around two weeks had passed"

;//鑑識員は一センチ程の蛆虫をつまみ上げてそう断言した。
Said forensics member after taking one of them with his fingers.

【Uozumi】
[v file="uoz0038"]
;//「二週間か……その間ずっと此処にホトケさんが居た訳はねえな」
"Two weeks, huh... It can't be that the coprse was lying here all the time"

;//殺害されたのは他の処なのだろう。
She was definitely killed in some other place.

;//切断された四肢と頭部が無造作に地面へ転がっているのだが、肝心の胴体が見当たらない。
Dismembered limbs and head are lying on the earth, but torso is nowhere to be found.

【Uozumi】
[v file="uoz0034"]
;//「おう、胴体は何処だ？」
"Where's her torso?"

【Forensics Member】
[v file="kan0005"]
;//「まだ発見されていないようです」
"We haven't discovered it yet"

【Uozumi】
[v file="uoz0035"]
;//「そうか」
"I see"

;//池の中に放り込まれでもしていたら面倒だな-
If the criminal thrown the torso into the pond, it will take time to find it.

;//もう此処を調べる必要はないだろう。
Nothing interesting left in here.
