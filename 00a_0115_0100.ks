[bgload file="bg02a.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm10a"]

【Reiji】
[v file="rei0021"]
;//「やれやれ……」
"Well..."

;//事務所に戻るなり、己は大きく溜息を吐いて椅子に身体を投げ出した。
Back in the office I heaved a sigh and splashed down into my armchair.

;//昨日までの事件の顛末を警察に報告した後、すぐに次の仕事を依頼されたのだ。
I told the police all details about current case, and immediately recieved the following.

【Reiji】
[v file="rei0022"]
;//「あの野郎、元同僚とは云え己を便利屋か何かと勘違いしているんじゃないのか？」
"I wonder, why that bastard thinks I'm his errand boy even after I quitted police?"

;//煙草を咥えたままひとしきり愚痴りながら、新たに受け取った書類を確認する。
Grumbling with a cigarette in my mouth, I was examining the documents on the next case.

【Reiji】
[v file="rei0023"]
;//「……世の中は相も変わらず殺しばかり、か」
"It feels like nothing happens in this world, except murders..."

;//仕事が多くて涙が出てくる。
I got so much work I wanna cry.

;//三鷹の下連雀で拳銃発砲事件があり、町工場の工場長が殺されたらしい。
An incident occured in Mitaka. Murder of the director of some local factory.

;//ヤクザ者の仕業だろうと己は当たりをつける。
This has not been without yakuza, I guess.

;//戦争から十年が過ぎ、戦災からの復興は進み続けて更なる発展を遂げようとしている。
It's been 10 years since the war ended. Country restoration continues, moreover you can even see some growth.

;//大工場や新興住宅地などを建てるには土地があまりにも足りない。
However there's not enough space to build new residential areas and large factories.

;//住人が居なくなれば土地が奪えるなど、短絡的思考も甚だしい。
Some people must be thinking that getting rid of landowner allows to take over his property.

【Reiji】
[v file="rei0024"]
;//「……とは言え、裏付けも取らないとならないからな……」
"Nonetheless I need a proof..."

;//立ち上がり、外套を羽織る。
I get out of the chair and put on my coat.

;//寒空の下外を歩き回るのは好きではないが、そもそも探偵とはそういうものだ。
I hate walking outside in cold weather, but that's exactly what detectives do.

;//まずは現場周辺の聞き込み。そして被疑者の絞り込みと逮捕に向けての調査か。
First I need to examine the areas nearest to the crime scene. Then narrow down the suspect pool and catch the criminal.

;//今年も忙しくなりそうだな。
Looks like this year will be tough one.

[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0118_0100.ks"]
[end]
