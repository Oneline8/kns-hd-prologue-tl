[bgload file="bg63b.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm10a"]

;//新宿へやってきた。
I've arrived at Shinjuku.

;//相変わらず此処は人で溢れている。
Lots of people around, just as always.

;//己は群衆の合間を縫うようにして先へ行く。
Weaving my way through a crowd I proceed forward.

;//紀伊國屋の横を抜け、伊勢丹の裏手の細い路地へ。
After passing Kinokuniya I head to a narrow alley behind the Isetan Department Store

[bgload file="bg01b.png"]
[effect type = 0 time = 1000]

;//此処が己の事務所だ。
Here's my office.

;//駅からも近く、外からの見た目も良いので気に入っている。
I like the fact it's located close to the station and looks good on the outside.

[bgload file="bg02g.png"]
[effect type = 0 time = 1000]

;//……中の見た目は御世辞にも良いとは言えないが。
...However the same can't be used to describe the inside.

;//外套をソファの上に放ると、白い埃が舞い上がる。
When I throw my coat on the sofa, a cloud of dust rises.

【Reiji】
[v file="rei0034"]
;//「ついこの間紫が片付けた筈なんだがなあ」
"I was sure that Yukari cleaned the place recently"

;//世の中にはまだまだ不思議な事があるようだ。
Seems, something extraordinary still happens in this world. 

;//椅子に座り、パンを頬張りながら業務日誌を書く。
I sat in my chair and began filling in my investigation notebook while eating bread.

;//三鷹の発砲事件については、背後関係が徐々に明らかとなってきた。
Looks like, the shooting incident in Mitaka is becoming more clear little by little.

;//やはり被害者が経営していた町工場周辺では、とある組織-当地の暴力団が積極的に土地を買い漁っていた。
As I expected, some local yakuza group was buying up land around the factory the victim owned.

;//被害者は最後まで土地を売る事に抵抗していたらしい。
And it seems, owner was agaist selling his land to the bitter end.

;//そしてその辺りには、夏頃に大きな工場が建てられると云う噂もあった。
Moreover, there've been some rumors about a new factory construction nearby.

;//この事が示すものとは-
Which means----

【Reiji】
[v file="rei0035"]
;//「実行犯を見つけ出すのは面倒だな……」
"It will be difficult to identify the criminal..."

;//犯人は暴力団の内部の人間だろう。連中は身内をしっかりと庇い立てして証拠を外に洩らさない。
He must be a member of yakuza. These guys always cover each other and leave no clues. 

;//警察へ行って当該組織の人物表でも見せてもらおうか。幸いその程度の関係はまだある。
Maybe I should get the suspects list from police. Fortunately I'm still able to do that. 

;//下っ端で最も鉄砲玉に使われそうな人間を洗い出して鎌を掛ける。
Identify a possible executer and get from him the name of the contractor.

;//警察官では許可されないだろう違法ぎりぎりの捜査でも、一介の探偵ならば可能だった。
It's very unlikely that police officer can get permission for such illegal activity, but for common detective it's a piece of cake.

;//-尤も、危険は警官と比べものにならない程大きいのだが。
On the other hand this will be dangerous.

;//先程の井の頭公園での出来事-気に懸かる。
I remembered the recent case in Inokashira park.

;//己は事件の書類を捲り、被害者の身内の調査書を眺めた。
Flipping through pages I stopped on the one that contains information about victim's surroundings. 

【Reiji】
[v file="rei0036"]
;//「ふむ-成程ね」
"Hmm... I see"

;//動機としては充分だろうか。
Could this be enough for a motive?

【Reiji】
[v file="rei0037"]
;//「さて-」
"Well..."

;//日没まではまだ時間がある。
I still have some time until dusk.

;//もう一箇所くらい何処か廻れそうだ。
I can take a stroll to another place.

[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0119_0100z.ks"]
[end]

;//そろそろ日が沈む。
The sun is almost down.

;//今日はこのまま事務所に泊まるとするか。
Should I stay in the office?

;//己はいつの間にか落書きだらけになっていた業務日誌の頁を破り、丸めて床に放り投げた。
I tore a page out and threw it on the floor.

【Reiji】
[v file="rei0038"]
;//「珈琲でも飲んでくるか」
"Should I go have some coffee?"

;//幸いこの辺りは飲食店には事欠かない。
Fortunately there's a lot of cafes in this area.

;//己は外套を羽織ると、床に散らかった塵を蹴飛ばしながら外へ出た。
Throwing on my coat, I leave the office pushing through the garbage on the floor.

[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0119_0200.ks"]
[end]
