[bgload file="bg59b.png"]
[effect type = 0 time = 1000]

;//陽が傾くまで現場検証と付近への聞き込みを行っていた魚住は、本庁へ戻る前に駅の北口へやってきた。
When he finished investigating the crime scene, on his way to MPD Uozumi stopped by the northern entrance of Kichijoji station. 

【Uozumi】
[v file="uoz0045"]
;//「……腹ぁ減ったからな」
"Man, I'm hungry"

;//わざとらしく呟く。
He mumbled.

;//死体を見た後だろうが、飯は食える。
Seeing a corpse doesn't bring him an appetite.

;//繊細な心など、十年も昔に戦場へ置き忘れてきてしまった。
All tenderness in his heart Uozumi had left on the battlefield 10 years ago.

;//刑事になってからは更にその傾向が顕著になっているように思う。
When he joined the police, the situation only worsened.

[bgload file="bg33b.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm04a"]

;//大柄な図体で細い路地を縫うように歩き、馴染みの店の前へ。
After passing a narrow lane, Uozumi came to familiar cafe.

;//此処へ来れば仕事の事は忘れられた。
In this place he could forget one's troubles.

;//少なくとも凄惨な遺体の事などは、絶対に話せないのだから。
At least about mutilated corpses. Because he couldn't talk about them here.

[se loop=0 file="se016"]

[bgload file="bg34b.png"]
[effect type = 0 time = 1000]

[bgset]
[charload num = 0 file = "haz0110b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0023"]
;//「いらっしゃい、魚住くん」
"Welcome, Uozumi-kun"

【Uozumi】
[v file="uoz0046"]
;//「-おう」
"Yep"

[bgset]
[charload num = 0 file = "haz0110b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//微かに声を出し、カウンタのスツールに腰掛ける。ぎしり、と厭な音が響いた。
Giving a faint answer he sat in front of the counter. His stool made an unpleasant sound.

[bgset]
[charload num = 0 file = "haz0101b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0024"]
;//「珈琲でいいかしら？」
"Would you like coffee?"

【Uozumi】
[v file="uoz0047"]
;//「ああ」
"Yeah"

[bgset]
[effect type = 0 time = 500]

;//杏子が背中を向ける。魚住はただ凝と彼女の背で揺れる髪を見つめていた。
Kyoko turned around, and Uozumi fixed his eyes on her beautiful bouncy hair.

[bgset]
[charload num = 0 file = "haz0205b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0025"]
;//「-今日もお仕事だったの？」
"Have you been working today?"

【Uozumi】
[v file="uoz0048"]
;//「ああ」
"Yeah"

[bgset]
[effect type = 0 time = 500]

;//短く答え、煙草を取り出す。そうでもしないととてもではないが間が持ちそうになかった。
Giving another short answer, he took a cigarette. Without smoking he couldn't get into the mood.

;//やがて煙草の匂いの中に、芳醇な珈琲の香りが混じってきた。
Soon the smell of tobacco mixed with the coffee aroma. 

;//この空気が魚住は好きだった。騒々しくなく、時間がゆっくりと流れているような気がする。
Uozumi liked this kind of atmosphere. It was so quiet and calming, that you could feel how time slowly passes.

;//始終走り続けていた魚住にとって、この月世界は唯一心安まる場所であった。
For a person as busy as him, this cafe was the only place, he could feel relieved.

[bgset]
[charload num = 0 file = "haz0101b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0026"]
;//「おまちどうさま」
"Thank you for waiting"

[bgset]
[effect type = 0 time = 500]

;//かちゃり、と前にカップが置かれる。
She put a cup of coffee in front of him.

【Uozumi】
[v file="uoz0049"]
;//「おう」
"Thanks"

;//砂糖壺を引き寄せ、何匙も砂糖を掬っては珈琲の中へ入れていく。
He took a sugar bowl and put a few spoons into his drink.

;//甘党と云う程ではないが、何も入れない珈琲の苦みは少々苦手だった。
It didn't mean Uozumi liked sweet, he just didn't like coffee's bitterness.

;//だからといって酒を飲むわけにもいかない。まだ勤務時間内なのだ。
And he couldn't drink sake, because he was still at his work.

[bgset]
[charload num = 0 file = "haz0201b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0027"]
;//「トーストくらいしかできないけれど、食べる？」
"At the moment I have only toasted bread, but want some?"

【Uozumi】
[v file="uoz0050"]
;//「……ああ」
"Sure"

[bgset]
[charload num = 0 file = "haz0201b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//さすがに此処で和食を頼む訳にもいくまい。
It was obvious, that there's no japanese food in here.

[bgset]
[charload num = 0 file = "haz0205b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0028"]
;//「少し待っていてね」
"Alright, give me a moment"

[bgset]
[effect type = 0 time = 500]

;//暫くしてパンの焦げる香ばしい匂いが漂ってくる。
And soon the aroma of toasted bread floated in the air.

[bgset]
[charload num = 0 file = "haz0101b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0029"]
;//「-昔は魚住くん、日本茶ばかり飲んでいたわよね」
"I remember when you used to drink only a japanese tea"

【Uozumi】
[v file="uoz0051"]
;//「-そうだな」
"It was long ago"

[bgset]
[charload num = 0 file = "haz0101b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//言葉少なに答える。
He answers shortly.

[bgset]
[charload num = 0 file = "haz0110b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0030"]
;//「好みが変わったのかしら？」
"Have your tastes changed?"

【Uozumi】
[v file="uoz0052"]
;//「……かもしれねえな」
"...It looks that way"

[bgset]
[effect type = 0 time = 500]

;//バターが塗られたトーストをひと囓りする。
He puts some butter on bread and takes a bite.

[bgset]
[charload num = 0 file = "haz0113b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0031"]
;//「ふふっ-」
"Hehe----"

;//杏子は微笑みながらもう一杯珈琲をカップに注いだ。
She pours him another cup, smiling.

[bgset]
[charload num = 0 file = "haz0101b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0032"]
;//「あたしも昔はお茶ばかりだったわ。珈琲を飲み始めたのは、そう-このお店に来るようになってからかしら」
"Earlier I also used to drink only tea. I started to drink coffee, when I opened this cafe."

[bgset]
[effect type = 0 time = 500]

;//懐かしそうな-寂しそうな目をして杏子が店内を見回す。
Kyoko looked around the place. Her eyes were filled with tenderness and sadness.

【Uozumi】
[v file="uoz0053"]
;//「……昔の話はやめろ」
"...Enough about the old times"

;//魚住は低い声を発して杏子の言葉を遮った。
Uozumi cut her thought with in a low voice.

[bgset]
[charload num = 0 file = "haz0201b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0033"]
;//「……そうね。ごめんなさい」
"...You're right. Sorry"

[bgset]
[effect type = 0 time = 500]

;//杏子の声にさほど気落ちした雰囲気がみられなかった事に魚住は安堵した。
He found no sign of upsetting in her voice and relieved.

;//昔の話はあまり良いものではない。
'Talking about past is no good' he thought.

;//思い出と云うものは時が経つにつれ美化されるが、魚住たちの過去は時が過ぎても色褪せる事無く、益々闇の部分を濃くしているように思える。
Usually memories become brighter with time, but Uozumi and his friends had everything other way around.

;//杏子や時坂の過去は特に-
Especially it applied for Kyoko and Reiji.

;//その中にあって、魚住だけはまだ幸せなのかもしれなかった。
Among them Uozumi might be the happiest.

;//失ったものが少ないから。
Because he still haven't lost anyone he really cares about.

【Uozumi】
[v file="uoz0054"]
;//「-帰る。邪魔したな」
"I'm going back. Sorry for trouble"

[bgset]
[charload num = 0 file = "haz0101b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Kyoko】
[v file="kyo0034"]
;//「邪魔だなんて-また来てね、魚住くん」
"No trouble at all... Please, come again, Uozumi-kun"

[bgload file="bg33c.png"]
[effect type = 0 time = 1000]

;//杏子に送り出され、暗くなった外へ出る。
They exchanged goodbyes, and Uozumi went outdoors. It was dark outside.

;//気の早い酔客がふたり、ふらふらと千鳥足で魚住の往く手を遮った。
Two staggring drunkards blocked his path.

【Uozumi】
[v file="uoz0055"]
;//「邪魔だ！」
"Out of my way!"

;//魚住は一喝し、怯んだ酔いどれ男を押し退けて吉祥寺駅へと向かった。
He shouted pushing a frightened drunkard and headed towards the station.

;//まったく-酒を呑みたい気分なのは俺の方だ。
'Damn, I could use a drink, too' he thought.

;//しかし現在起こっている事件から逃避していた時間は既に終わっているのだ。
But he no longer had time for that.

;//魚住は外套の襟を立て、冷え込みが増してきた二月の夜の街をひとり歩き続けた。
Since it was February, weather was getting colder. Uozumi raised his collar and continued walking through the night streets.

[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0301_0100.ks"]
[end]
