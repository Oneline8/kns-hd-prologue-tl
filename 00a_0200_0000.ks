[bgload file="eye_base01.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm16"]

;//嫌い。
I hate it.

;//嫌い嫌い。
I hate everything.

;//人間が嫌い。自分が嫌い。この世の全てが嫌い。
Other people. Myself. The whole world.

;//そしてなによりも、母親が嫌いだ。
But the one I hate the most is my mother.

;//どうして私はこのような、穢れて澱んだ世界に生まれてきてしまったのだろう。
Why? Why I'm the one to be born in this rotten world?

;//それは母親の罪。穢れた魂の輪廻が全てを狂わせたのだ。
It is all her fault. She broke the samsara wheel of sinful souls.

;//私は憐れだ。私は不幸だ。
I'm so helpless. I'm so miserable.

;//私だけじゃない。きっと他の人たちだって-級友だって、先生だって、犬だって、猫だって、みんな不幸に違いないのだ。
And it's not only me. Other people, classmates, teacher, dogs and cats, all of them are miserable too.

;//でも-みんなと私にたったひとつだけ違う処がある。
But there's one thing that makes me different.

;//私にはあの人がいるから。
I have her.

;//＄　あの人がいるから、私は生きていける。
$If we're together, I can live on.

;//あの人の瞳が、あの人の髪が、あの人の仕草ひとつひとつが-
Her eyes, hair, gestures----

;//あの人の全てが、私を落ち着かせる。
Everything in her makes me relieved.

;//あの人がいるから、未来に希望を持つ事が出来る。
While she exists, I can look forward with hope.

;//あの人がいるから、もしかしたら私は不幸などでは無いのかもしれない。
While she exists, maybe I'm not that helpless.

;//＄　だってこれから、あの人に逢うのだから-
$Because I'm the one who met her----

[bgload file="bg74b.png"]
[effect type = 0 time = 1000]

;//-月が綺麗だ。
The moon is beautiful, isn't it?

;//あの人は月が好きだ。
She likes the moon.

;//だから私も、月が好きなのだ。
That's why I like it, too

;//月明かりの下、あの人が現れた。
The moment this girl appeared in the moonlight

;//＄　私は私の身体が熱くなっていくのを感じていた。
$I felt the heat flowing through my body.

[bgmstop]

[bgload file="eye_base01.png"]
[effect type = 0 time = 1000]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0229_0000.ks"]
[end]
