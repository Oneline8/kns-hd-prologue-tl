[bgload file="bg09b.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm02a"]

;//三鷹での調査を終え、自宅に帰ってきた。
After I finished all my business in Mitaka I returned home.

;//紫も既に帰ってきているらしく、台所からは香ばしい匂いが漂ってくる。
It seems Yukari is back from school. I noticed a sweet smell coming out of the kitchen.

【Reiji】
[v file="rei0025"]
;//「ただいま、紫」
"I'm home, Yukari"

[bgset]
[charload num = 0 file = "yuk0303b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0085"]
;//「お帰りなさい」
"Welcome back"

;//台所から紫が顔を覗かせる。
Yukari popped up from the kitchen

;//どことなく顔色が悪いように見えた。
Her face looked somewhat pale

【Reiji】
[v file="rei0026"]
;//「どうした紫、風邪でもひいたのか？」
"What's the matter, Yukari, you got a cold?"

[bgset]
[charload num = 0 file = "yuk0305b.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0086"]
;//「そうではないけれど……」
"Not really..."

[bgset]
[effect type = 0 time = 500]

;//紫は沈んだ表情のまま食事の準備を続ける。
She continues to cook dinner in a foul mood

;//無理に聞き出す事もないだろう。何かあるのならば、そのうち紫の方から切り出してくるに違いない。
I decide not to ask her anything. When she's ready, Yukari will tell everything herself.

[bgload file="bg09c.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm02b"]

[bgset]
[charload num = 0 file = "yuk0605a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0087"]
;//「-兄さん」
"Nii-san..."

;//夕食を終え、案の定紫が神妙な面持ちで話し掛けてきた。
As I expected after we're done with food, she came talk to me.

【Reiji】
[v file="rei0027"]
;//「何かあったのか？」
"Something happened?"

[bgset]
[charload num = 0 file = "yuk0605a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//飲んでいた珈琲を置く。
I put aside my coffee.

[bgset]
[charload num = 0 file = "yuk0606a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0088"]
;//「ええ……部活の後輩のことで。昨晩から家に帰っていないらしくて心配で……」
"Yes... with a girl from my club. She hasn't been home since yesterday, so I'm worried about her..."

【Reiji】
[v file="rei0028"]
;//「家出じゃないのか？」
"Couldn't she willingly ran away from home?"

[bgset]
[effect type = 0 time = 500]

;//年頃の少女にはよくある事だ。年を経れば些細な事だと笑えるような理由でも深刻に捕らえてしまうのが若さと云うものだろう。
For girls of her age it's a common thing. No matter how ridiculous issue is, youngsters can make a scandal of it. That's how they are.

;//こんな事を考えるとは、己も随分と老け込んでしまったようだ。
I must be getting old if I already think like that.

【Reiji】
[v file="rei0029"]
;//「親御さんは捜索願を出したのか？」
"Did her parents contact the police?"

[bgset]
[charload num = 0 file = "yuk0305a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0089"]
;//「それが判らないの。出しているとは思うんだけれど……」
"I don't know. But I think, they did..."

【Reiji】
[v file="rei0030"]
;//「だとしたら-後は警察の仕事だ」
"In that case it's a police matter now"

[bgset]
[charload num = 0 file = "yuk0604a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0090"]
;//「兄さん！？」
"Nii-san!?"

;//非難がましく己をきつく睨み付けてくる紫を諭そうと口を開く。
With displeased look she reproached me.

【Reiji】
[v file="rei0031"]
;//「いいか、一介の探偵に出来る事なんて極僅かなんだ。行方不明者の捜索ならば、人数を多くかけられる警察の方がずっと確実だ」
"You know, some private detective like me won't be much help. You need a lot of people to find a missing person. That's something only police can do"

[bgset]
[charload num = 0 file = "yuk0607a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0091"]
;//「そうかもしれないけれど……」
"Maybe you're right..."

;//猶も紫は食い下がろうとしたが、理解したのだろう。急に大人しくなった。
Yukari still tries to argue, even though she understands. But then she quickly calms down.

【Reiji】
[v file="rei0032"]
;//「そう心配しなくてもすぐに帰ってくるだろうさ」
"No need to make a fuss. I think she'll be back soon"

[bgset]
[charload num = 0 file = "yuk0306a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0092"]
;//「ええ……」
"Okay..."

【Reiji】
[v file="rei0033"]
;//「己も仕事の合間に調べられるだけ調べてみるから心配するな」
"Don't worry. When I happen to have some free time I'll try to find anything out"

[bgset]
[charload num = 0 file = "yuk0607a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0093"]
;//「……宜しくお願いね、兄さん」
"Please do, Nii-san"

[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0119_0100.ks"]
[end]
