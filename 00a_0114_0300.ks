[bgload file="aspect01.png"]
[effect type = 0 time = 1000]

[bgset]
[effect type = 0 time = 500]

[bgset]
[effect type = 0 time = 500]

[bgload file="bg02a.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm10a"]

;//事務所の鍵が開いていたのである程度は予想していたが、まさか兄の下着をしげしげと眺めているとは思いもよらなかった。
When I found the door to my office is opened, I was expecting anything but Yukari staring at my underpants.

【Reiji】
[v file="rei0001"]
;//「……何をしているんだ、紫……？」
"What are you doing, Yukari?"

[bgset]
[charload num = 0 file = "yuk0103a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0064"]
;//「洗って汚れが落ちるかどうか考えていたのよ」
"I was thinking if some washing can fix this stain"

;//しれっと紫は言うと、手慣れた様子で己の下着を畳んでいく。
She calmly replied, continuing to sort my stuff.

[bgset]
[charload num = 0 file = "yuk0404a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0065"]
;//「……少しは自分でお掃除したらどうですか、兄さん？」
"Tell me, Nii-san, why can't you sometimes clean up the office yourself?"

【Reiji】
[v file="rei0002"]
;//「……忙しいんだよ」
"I got a lot going on"

[bgset]
[effect type = 0 time = 500]

;//言い訳がましく弁解すると、己は上着を椅子の背もたれに引っかけて腰を下ろした。
Making an excuse, I took off my coat, put it on chair back and sat down.

;//事務所内を見回す。随分と綺麗になっている。
I looked around. Everything just seemed cleaner.

;//いや、数週間前の状態に戻っただけか。
Or maybe I should say, that everything reverted to few weeks ago condition.

;//我ながらよく汚したものだ。
She's right. Cleanliness is not my priority.

;//紫に来て貰えなかったらこの事務所がどうなってしまうのか、想像もしたくない。
I don't even want to think of what the office would turn into, if Yukari hadn't keep coming here.

【Reiji】
[v file="rei0003"]
;//「-いつも済まないな」
"Sorry for troubles"

[bgset]
[charload num = 0 file = "yuk0103a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0066"]
;//「いいのよ。兄さんはお仕事で忙しいのでしょうから」
"Don't mind. I know that you're busy at work"

[bgset]
[effect type = 0 time = 500]

;//洗濯物を畳むと、紫は紙袋を取り出した。
She finished folding the clothes and took out a paper bag.

[bgset]
[charload num = 0 file = "yuk0401a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0067"]
;//「はい、中華まん。……少し冷めてしまったけれど」
"Here, I bought you nikuman. It's gotten a little cold though"

【Reiji】
[v file="rei0004"]
;//「構わないさ。いただくよ」
"It's fine. Thanks"

[bgset]
[charload num = 0 file = "yuk0401a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//丁度腹も減っていた処だ。中村屋の肉まんは冷えていても美味いのでなんら問題はない。
I was just feeling hungry. These steamed pork buns from Nakamuraya are tasty even when they got cold, so no problem here.

[bgset]
[charload num = 0 file = "yuk0102a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0068"]
;//「-兄さん、お仕事は順調？」
"Nii-san, how's your job going?"

【Reiji】
[v file="rei0005"]
;//「ん……ああ、一両日中には片が付きそうだ」
"Mm... Ah, I think I'll finish one case very soon"

[bgset]
[effect type = 0 time = 500]

;//中華まんを頬張りながら紫の問いに答える。
I said that with my mouth full.

;//年末から己はずっとひとつの案件に関わっていた。とある富豪の遺産相続に関係する殺人事件だ。
It's been a few weeks since I started investigation. The case is about murder connected to the inheritance of some rich man.

;//犯人の目星も付き、後は警察と連絡を取って奴を逮捕するだけだ。
I already aware of the criminal's motivation, the only thing left is contacting the police and arresting him.

[bgset]
[charload num = 0 file = "yuk0407a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0069"]
;//「……危険ではないの？」
"Isn't it dangerous?"

;//ぽつり、と紫が言葉を洩らした。
She blurted.

【Reiji】
[v file="rei0006"]
;//「ただの調査に危険な事なんてないさ」
"There's nothing dangerous about an ordinary investigation"

[bgset]
[charload num = 0 file = "yuk0407a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

;//軽い口調で己は返答した。
I replied calmly.

[bgset]
[effect type = 0 time = 500]

;//絶対に安全、とは言い切れない。この仕事は何処から危険が舞い込んでくるか判らないからだ。
Honestly, absolutely safe cases don't exist. On this job you never know, what may happen.

;//単なる浮気調査で、全身蜂の巣になって死んだ奴を己は知っている。迷い猫捜しであろうとも、危険と無縁である保証はないのだ。
I knew a guy, who got riddled with bullets during the case about cheating. So, even a search for a missing cat doesn't guarantee you'll be fine.

[bgset]
[charload num = 0 file = "yuk0105a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0070"]
;//「だったらいいのだけれど……」
"I'm glad, if it's true..."

;//あまり己の言葉を信用していないのだろう、紫は尚も不服そうに呟いた。
She muttered with disapproval. It's seemed she didn't believe my words.

;//信用を失っているのは己の責任なのだろうけれど。
The loss of her trust is weighing on my conscience.

【Reiji】
[v file="rei0007"]
;//「-帰るか、紫」
"Let's go home, Yukari"

[bgset]
[charload num = 0 file = "yuk0405a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0071"]
;//「お仕事はもういいの？」
"What about your work?"

【Reiji】
[v file="rei0008"]
;//「年が明けてから帰ってないからな。仕事の方は問題ない」
"I haven't been home since the beginning of the year. It'll be fine"

[bgset]
[charload num = 0 file = "yuk0101a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0072"]
;//「でしたら、これは兄さんが持ってくださいね」
"If you say so, then carry this"

[bgset]
[effect type = 0 time = 500]

;//紫が風呂敷に包んだ己の服を差し出してきた。
She lent me the laundry.

[bgset]
[charload num = 0 file = "yuk0105a.png"]
[charset num = 0 left = 400 top = 0]
[effect type = 0 time = 500]

【Yukari】
[v file="yuk0073"]
;//「幾ら冬とは云え、これだけ放置していたら臭ってきそうだもの」
"When you leave dirty clothes for so long, it begins to stink"

[bgset]
[effect type = 0 time = 500]

;//……これも勿論、己の責任なのだろうな。
...And of course this was also my fault.

[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0114_0400.ks"]
[end]
