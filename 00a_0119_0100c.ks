[bgload file="bg61b.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm01a"]

;//吉祥寺駅から南へ暫く歩けば、林の中にある池へ出る。
If you go to the south from Kichijouji Station, you will find a pond surrounded by trees.

;//此処が井の頭恩寵公園だ。
This place is called Inokashira Park.

;//平日の夕方、それに冬だと云う事もあるのだろうか、人影は疎らだった。
During the winter weeknight like this you won't see many people around.

;//当然ながら屋台も出ていない。たこ焼きでも食いたかったのに残念だ。
And of course you won't find any food carts. That's too bad, I wouldn't mind eating a takoyaki right now.

【Reiji】
[v file="rei0056"]
;//「さて-」
"Let's see----"

[bgmin time=2000 file="bgm07"]

;//別に己は此処へ休憩にやってきた訳ではない。現在調査中の発砲事件は、此処から程近い処が現場だ。
I have a good reason for being here. Currently I'm investigating the shooting incident, that occurred recently not far from here. 

;//木々の切れ目からその町工場が見える。
The factory where it happened can be seen through trees.

;//己は事前に受け取った資料に含まれていた現場の見取図を取り出し、現状と併せて確認する。
I took an area map from the pile of documents I received earlier, and began comparing it with the scenary.

【Reiji】
[v file="rei0057"]
;//「こりゃあ……厳しいな」
"That's harsh"

;//ヤクザ者の犯行と断ずるには尚早かもしれない。
Can't say for sure if it was the work of yakuza.

[bgmstop]

[se loop=0 file="se001"]

【Reiji】
[v file="rei0058"]
;//「ん……？」
"Hmm...?"

[bgload file="bg62b.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm07"]

;//不意に聞こえてきた水音に視線を転じる。
Suddenly I heard a splash of water and turned around.

;//池の中央辺りの水面に幾重もの輪が出来ていた。
There were ripples right at the center of the pond.

;//何かが落ちたのか……？
Have something just fallen into it...?

;//池の対岸に、外套の襟を立てて足早に歩く人影を見つけた。
On the opposite bank of the pond I noticed a briefly walking person in a coat with upraised collar.

【Reiji】
[v file="rei0059"]
;//「あいつは……」
"That person..."

;//何か怪しい。
Looks suspicious.

[bgload file="bg61b.png"]
[effect type = 0 time = 1000]

;//追いかけようとしたが、その男が向かう先を認めて足を止めた。
I was going to spy on him, but he spotted me and stopped.

;//あそこは-
That place----

[bgload file="bg62b.png"]
[effect type = 0 time = 1000]

;//もう一度池を見る。
I took another look at the pond.

;//既に水面の輪は消えていた。
The water surface was plain again.

;//中心があった処は、どう頑張った処で岸から届くような距離ではない。
Distance was too far, he couldn't throw anything from there and hit the center.

【Reiji】
[v file="rei0060"]
;//「……寒中水泳なんぞ出来る訳ねえだろ」
"...And it's very unlikely that anyone would swim during winter"

;//そう云うのは専門家に任せるとしよう。
I decided to leave this to matter to experts.

【Reiji】
[v file="rei0061"]
;//「……戻って他の資料をもう一度洗ってみるか」
"Should I go to my office and review materials on the case one more time?"

;//-特に被害者の身内を。
Especially the part about victim's surroundings.

[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0119_0100z.ks"]
[end]
[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0119_0200.ks"]
[end]
