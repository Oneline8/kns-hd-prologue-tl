[bgload file="eye_base02.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm09"]

;//-闇の中に一糸纏わぬ少女の肢体が白く浮かび上がる。
In the deep dark room there is a naked body of a girl.

;//仄かに色付いた胸の先端が僅かに上下している。
Her pale nipples are moving slightly.

;//少女は眠っていた-否、気を喪っていた。
She's asleep. No. Unconscious.

;//少女の左腕に触れる。程良い肉付きと弾力。申し分ない。
I touch her left arm. Suitable fullness and elasticity. There is nothing to complain about.

;//手首と肘を持ち、腕を広げさせる。少女は腋を締めるようにして微かに抵抗をみせた。
Holding her wrist and elbow, I straightened her arm, and felt a weak resistance.

;//無意識の事だろう。その証拠に彼女はまだ静かに寝息を発している。
It seems, she did it unconsciously. Her quiet breathing is still audible.

;//寝返りを打てぬよう、腰にベルトを廻して台に固定する。左腕も同様に、伸びきった状態で台の端に固定した。
To make sure, she won't move again, I tie her torso and her arm to the table.

;//-これでいい。
Much better.

;//部屋の片隅に打ち棄てられていた鉈を掴む。幾分か刃毀れが見受けられるが問題あるまい。
From the corner of the room I take a cleaver. Blade is a bit dull, but it doesn't stop me.

;//両手でしっかりと持ち、少女の肩に刃先で触れる。肉が僅かに沈み込むが、それで疵が付く筈もない。
Clutching the cleaver tightly in my hand, I touch the girl’s shoulder with the blade. A small bruise appears on her skin, but of course it isn't damaged.

;//＄　さあ-はじめよう。
$Shall we begin.

;//＄　鉈を大きく振りかぶった。
$I raise my hand.

[se loop=0 file="se032"]

[bgload file="blood01.png"]
[effect type = 0 time = 1000]

[bgload file="blood02.png"]
[effect type = 0 time = 1000]

[bgload file="blood03.png"]
[effect type = 0 time = 1000]

[bgload file="blood04.png"]
[effect type = 0 time = 1000]

[bgload file="te004a.png"]
[effect type = 0 time = 1000]

;//少女の、声にならない悲鳴が響き渡った。
A wild scream comes from her body.

;//鉈の先端が台板に食い込む。
The tip of the blade dug into the surface of the table.

;//少女の左腕がだらりと垂れ下がる。鉈に体重を掛け、力任せに押し込んでいく。
Her left arm now hangs freely. I press hard on the cleaver----

;//ぼとりと腕が床に落ち、赤い血液が噴き出した。
And it falls on the floor, a large steam of blood squirts from her wound.

;//少女は猶も声にならぬ声を発していた。身体ががくがくと跳ねている。
The girl is still screaming, her body convulses violently.

;//良かった-身体を固定しておいて。
I'm glad I strapped her down tight.

;//床に落ちた左腕を拾い上げる。未だ筋肉が痙攣していた。生暖かい血がぽたぽたと床に染みを作る。
I take her dismembered arm. The muscles are still twitching. Warm blood drips onto the floor.

;//真新しい手拭いで腕を丹念に拭く。手拭いは直ぐに真っ赤に染まった。
I carefully wipe it with a new towel, which immediately turns red.

;//この血は要らない。何枚も手拭いを使い、忌まわしい肉体から切り離された左腕は白い輝きを放つようになった。
I don't need this blood. Having it folded a several times, I bring the hand severed from the disgusting body to a state of dazzling whiteness.

;//丁寧に布で左腕を包み込む。後は-
And then carefully wrap it into cloth. What's left is----

;//痙攣が弱くなった肉体を見る。少女の口から叫び声はもう出てこない。代わりに空気の抜けるような空虚な音を微かに発していた。
I look at the body that still twitches slightly. She's no longer screaming. Now only a faint sound is heard, as if air is coming out.

;//少女の脚の方へ回り込む。両脚を広げるが、それに抵抗するような力は残されていない。台の上が血ではない液体で濡れていた。失禁したようだ。
Now I turn to the girl's legs and spread them. She seems to have no strength left for resistance. The surface of the table became wet from some kind of liquid, and it wasn't not blood. Looks like she peed herself.

;//薄く陰毛が生えそろった陰部を指で押し広げる。まるで前戯をされたかのように其処は濡れそぼっていた。陰唇が時折ぴくりと蠢く。
I stretch the genital area with my fingers. As if from foreplay, she's wet abundantly. Her labia twitches occasionally.

;//厭わしい-
Disgusting----

;//出刃包丁を手に取り、刃を少女の下腹部に宛て交う。
I take my knife and point it at the girl's abdomen.

[se loop=0 file="se049"]

[bgload file="blood11.png"]
[effect type = 0 time = 1000]

[bgload file="blood12.png"]
[effect type = 0 time = 1000]

[bgload file="blood13.png"]
[effect type = 0 time = 1000]

[bgload file="blood14.png"]
[effect type = 0 time = 1000]

[bgload file="blood15.png"]
[effect type = 0 time = 1000]

;//魚の腑を取り除くときのように刃を滑らせる。臍から下に赤い筋が出来る。
Acting as if gutting a fish, I run my blade, exposing the red muscles below her belly button.

;//陰核の上まで裂き、疵口を広げて中に手を挿れる。
When it come to her clitoris, I expand it and insert my arm inside.

[se loop=0 file="se056"]

[bgload file="blood05.png"]
[effect type = 0 time = 1000]

[bgload file="blood06.png"]
[effect type = 0 time = 1000]

;//＄　少女は何の反応も示さない。
$The girl shows no reaction.

;//噴き出す血潮も勢いがない。未だ事切れていない事を願いつつ、体内に潜り込ませた手で女にしか無い器官を無造作に掴む。
Bleeding has weakened either. Wishing that she is still alive, I feel for the internal genitalia.

[se loop=0 file="se056"]

[bgload file="blood05.png"]
[effect type = 0 time = 1000]

[bgload file="blood06.png"]
[effect type = 0 time = 1000]

;//手の中に収まってしまう程小さな-子宮。其れを体外へ引き摺り出そうとするも、あちこちが未だ繋がっている為上手くいかない。
Uterus is small enough to fit in my hand. I try to pull it out, but it's still connected to her body.

[se loop=0 file="se057"]

[bgload file="blood07.png"]
[effect type = 0 time = 1000]

[bgload file="blood08.png"]
[effect type = 0 time = 1000]

[bgload file="blood09.png"]
[effect type = 0 time = 1000]

;//＄　切れ目に包丁の先端を差し入れ、膣から子宮を切り離す。
$I insert a knife inside and separate the uterus from the vagina.

;//他にも数箇所を切り、ようやく子宮を取り出した。
After making incisions in some other places, I finally take it out.

;//白くぶよぶよとしたそれは、とても子を育む器官とは思えなかった。唯の臓物-肉塊に過ぎなかった。
White and soft, it's completely unlikely that it can give a birth. It's nothing more than a chunk of meat.

[se loop=0 file="se045"]

;//＄　床に其れを叩きつける。こんな穢れた臓物など要らぬ。
I throw it on the floor. I don't need this crap.

[bgload file="te004b.png"]
[effect type = 0 time = 1000]

;//卵を手に取った。表面が黒く塗られた鶏の卵。結ばれていた紐を千切り、腹の裂け目に押し込む。
I take a chicken egg painted in black and put inside the girl's abdomen.

;//そして腹の中で握りつぶす。中身を抜いて空だったのだろう、殻はさしたる抵抗もなく砕けてしまった。
And then I break it with my hand. It's empty inside, so it's shell cracks easily.

[bgload file="kara01.png"]
[effect type = 0 time = 1000]

[bgload file="kara02.png"]
[effect type = 0 time = 1000]

;//＄　腕を引き抜く。掌に食い込んだ殻の破片を他の臓器諸共身体の中に戻し、腹を抑える。
$I take out my hand and start putting back the fragments of the shell, that were stuck to my palm.

;//畳針に糸を通し、疵口を縫い合わせていく。
After that I take a needle with thread, and start sewing up girl's abdomen.

;//ぷつり、ぷつりと針を刺す。歪な十字が腹を横切る。
The needle enters her skin with a distinctive sound. Now a curved cross crosses her abdominal cavity.

;//-これでいい。
Excellent.

[bgload file="te004.png"]
[effect type = 0 time = 1000]

;//元に戻った死体に、満足気に頷く。
Now she's as good as new. I'm satisfied.

;//後は此を運ぶだけだ。
The only thing left is dumping her body.

;//本を捲る。この者に与えられるべき罰は-
I'm flipping through a book. This person deserved punishment.

;//＄　黒い布で少女だった物を包む。
$I wrap girls remains into a black cloth.

;//さて-往こう。
Well, I'll be going then.

[bgmstop]

[bgload file="eye_base02.png"]
[effect type = 0 time = 1000]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0302_0100.ks"]
[end]
