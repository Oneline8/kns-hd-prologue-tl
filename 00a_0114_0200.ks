[bgload file="bg63a.png"]
[effect type = 0 time = 1000]

[bgmin time=2000 file="bgm02a"]

;//武蔵境から中央線に乗り、新宿まで紫はやってきた。
From Musashi-Sakai, Yukari took the Chuo Line to Shinjuku.

;//規則で繁華街への立ち入りは禁止されている。
Entering central areas was forbidden by the academy rules.

;//それでも紫は此処へ来た。駅からほど近いビルディングが並ぶ一角に、兄が居る探偵事務所があるからだ。
Nevertheless she came here. No far from the station, among other buildings, her brother's detective agency was located.

;//東口から駅を出て、中村屋で中華まんを買い、細い路地に入る。
She bought some nikuman at Nakamuraya and ducked into a narrow alley.

[bgload file="bg01a.png"]
[effect type = 0 time = 1000]

;//瀟洒なビルディングの郵便受けに『時坂探偵事務所』と記されている。
In front of her appeared an elegant building. "Tokisaka Detective Agency" was written on the mailbox.

;//此処が紫の兄、時坂玲人が居る探偵事務所だ。勿論この建物の一室を借りているだけに過ぎないのだけれど。
Her brother Reiji worked here. Of course, he rented only a single room.

[se loop=0 file="se018"]

【Yukari】
[v file="yuk0058"]
;//「兄さん、居ますか？」
"Nii-san, are you here?"

;//返事がない。仕事に行っているのだろう。紫は鞄から真鍮製の鍵を取り出すと鍵穴に差し込んだ。
No answer followed. He's probably out on runs. Yukari took the key from her bag and opened the door.

[se loop=0 file="se009"]

[bgload file="bg02f.png"]
[effect type = 0 time = 1000]

【Yukari】
[v file="yuk0059"]
;//「はあっ……」
"Ugh..."

;//室内を見るなり、紫は大きく溜息を吐いた。
She looked across the room and heaved a sigh.

;//紙屑が散らばり床はほとんど見えなくなっている。
The floor was littered with papers and barely visible.

;//テーブルに置かれた硝子のグラスには、何時からあるのか判らない黄土色に濁った液体が入っている。
On the table there was a glass with some murky yellow liquid.

;//皿の上に無造作に残されているトーストは、もしかしたら黴びているのかもしれない。
A small piece of bread left on the plate was covered with mold.

;//前回此処に来たのが年末だったから、僅か二週間程でこの有様と云う事になる。
Last time Yukari came here the year before. It's been just two weeks since then, and the room was already in this state.

【Yukari】
[v file="yuk0060"]
;//「少しは自分で片付ける事もすればいいのに……」
"If you'd been able to clean up a bit..."

;//と愚痴を零した処で相手が居ないのだから仕方がない。
Her mumble was addressed to someone who wasn't present.

;//紫は鞄をソファの上に置き、室内の掃除を始める事にした。
Leaving the bag on sofa, Yukari started cleaning up.

;//まずは床に散らばった紙屑を一箇所に掻き集める。
First, she decided to gather all papers from the floor into a single stack.

;//もしかしたら必要な書類が在るかもしれないから、おいそれと棄てるわけにもいかない。
Some of them must be important, so she couldn't just throw them away.

;//紫は床にしゃがみ込み、紙屑を一枚ずつ皺を伸ばして丁寧に折り畳んでいく。
Squatting, she checked papers carefully and sorted them.

;//『殺し』や『誘拐』等の不穏当な単語が幾つも書き殴られている。
Murder, kidnapping and other similar words were mentioned there.

;//相変わらず兄はそう云う類の仕事ばかり受けているようだ。
It seemed, her brother still takes only this kind of work.

;//もっと安全な-例えば迷い犬捜しのような仕事をしていて欲しいと紫は願う。
Yukari always wanted him to take safer jobs such as looking for missing dogs.

;//唯でさえ兄は学者肌な処があり、荒事には向いていないと云うのに。
Although even in ordinary cases he tends to use his analytical mind. The role of aragoto doesn't fit him.

;//軍隊に居たり警察官だった時期もあるけれど、それでも兄は線が細くて優男然としているのだから。
In past he served in the army and worked in police, yet ended up staying a gentle and delicate person.

;//紙束を机の上に置き、次はテーブル上の物を片付ける。
Yukari left the paperwork on the table and moved on.

;//黴びたトーストを皿ごと取り、屑籠へ。グラスも中の液体を流しに捨てて念入りに洗う。
The mouldy bread went into trash. Then she poured out the contents of the glass and washed it.

;//洗い場には卵の殻が幾つも転がっていた。もしかしたらグラスの中の物は卵なのかもしれない。
Since there were eggshells in the sink, that yellow liquid must be an egg. 

;//生で呑んだのだろうか、あまり考えたくない。
Looks like her brother drank it raw.

[se loop=0 file="se060"]

;//殻を拾い上げて屑籠へ。強く握った所為か、ぱりっと音がして殻が砕けた。
Yukari gathered and threw away the eggshells. Falling, they made a crispy sound.

【Yukari】
[v file="yuk0061"]
;//「あとは-」
"And now..."

[bgload file="bg02a.png"]
[effect type = 0 time = 1000]

;//ある程度片付いた部屋を見回すと、片隅に無造作に積まれた洗濯物が目に入った。
Once more she took a look at the room and noticed some clothes lying in the corner.

;//さすがに此処では洗濯が出来ないから、家に持って帰るしかないだろう。
There weren't any means of washing in here, so she had to take it home.

【Yukari】
[v file="yuk0062"]
;//「……臭わなければいいけれど」
"I wish it didn't smell..."

;//シャツや下着を畳みながら紫はぼやいた。
She muttered, folding t-shirts and underwear.

[se loop=0 file="se009"]

;//事務所の扉が開き、紫は顔を上げた。
At that moment the door flew open. Yukari raised her head.

【Yukari】
[v file="yuk0063"]
;//「あ……お帰りなさい、兄さん」
"Ah... Welcome back, Nii-san"

[bgmstop]

[fill color=0x0]
[effect type = 0 time = 1000]

[bgmstop]

[exec file = "00a_0114_0300.ks"]
[end]
